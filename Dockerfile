FROM eclipse-temurin:17-jdk-jammy
WORKDIR /app
ENV ARTHAS_VERSION=3.6.7
EXPOSE 8080 7777
# RUN wget https://github.com/alibaba/arthas/releases/download/arthas-all-${ARTHAS_VERSION}/arthas-tunnel-server-${ARTHAS_VERSION}-fatjar.jar -O app.jar
COPY arthas-tunnel-server-3.6.7-fatjar.jar ./app.jar
# CMD ["java", "-jar", "app.jar"]
CMD ["sh", "-c", "exec java -jar ${JAVA_OPTS} app.jar"]
