package com.lgypro;

import com.lgypro.entity.User;
import com.lgypro.mapper.EmployeeMapper;
import com.lgypro.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class MyBatisDemoTest {
    private static SqlSessionFactory sqlSessionFactory;
    private SqlSession sqlSession;

    private UserMapper userMapper;

    @BeforeAll
    static void initAll() throws IOException {
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
    }

    @BeforeEach
    void initEach() {
        sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    @AfterEach
    void destroyEach() {
        sqlSession.close();
    }

    @Test
    void testGetUserByName() {
        User michael = userMapper.getUserByName("michael");
        Assertions.assertEquals("123", michael.getPassword());
    }

    @Test
    void testCheckCredential() {
        Assertions.assertAll(
                () -> Assertions.assertNotNull(userMapper.checkCredential("rose", "roserose")),
                () -> Assertions.assertNotNull(userMapper.checkCredential("lisi", "lisi123")));
    }

    @TestFactory
    List<DynamicTest> testCheckCredentialByMap() {
        return Arrays.asList(
                DynamicTest.dynamicTest("checking rose", () -> Assertions.assertNotNull(userMapper.checkCredentialByMap(Map.of("username", "rose", "password", "roserose")))),
                DynamicTest.dynamicTest("checking lisi", () -> Assertions.assertNotNull(userMapper.checkCredentialByMap(Map.of("username", "lisi", "password", "lisi123"))))
        );
    }

    @Nested
    @Disabled("skip DML tests")
    class InsertOperations {
        @Test
        void testInsertUser() {
            User user = new User();
            user.setUsername("cliton");
            user.setPassword("98372");
            user.setGender('♂');
            user.setEmail("cliton@example.com");
            int updateCount = userMapper.insertUser(user);
            Assertions.assertEquals(1, updateCount);
            System.out.println("id = " + user.getId());
        }

        @Test
        void testInsertEmployee() {
            Map<String, Object> properties = new HashMap<>();
            properties.put("id", 8510);
            properties.put("name", "jordan");
            properties.put("job", "SALESMAN");
            properties.put("managerId", 7782);
            properties.put("hireDate", LocalDate.now());
            properties.put("salary", 8000.0);
            properties.put("commission", 1500.0);
            properties.put("departmentId", 20);
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            mapper.insertEmployee(properties);
        }
    }

    @Nested
    class SelectOperation {
        @Test
        void testGetUserById() {
            User user = userMapper.getById(27);
            Assertions.assertNotNull(user);
            Assertions.assertEquals(27, user.getId());
            sqlSession.commit();
            user = userMapper.getById(27);
            System.out.println(user);
        }

        @Test
        void testGetCount() {
            int count = userMapper.getCount();
            System.out.println(count);
            Assertions.assertTrue(count > 0);
        }

        @Test
        void testGetByIdToMap() {
            Map<String, Object> user = userMapper.getByIdToMap(22);
            System.out.println(user);
        }

        @Test
        void testAllToMap() {
            List<Map<String, Object>> users = userMapper.getAllToMap();
            users.forEach(System.out::println);
        }

        @Test
        void testAllToMap2() {
            Map<Integer, Map<String, Object>> users = userMapper.getAllToMap2();
            users.entrySet().forEach(System.out::println);
        }

        @Test
        void testAuthUser() {
            Map<String, String> loginCredential = Map.of("username", "jack",
                    "password", "jack123");
            Assertions.assertNotNull(userMapper.auth(loginCredential));
        }
    }

    @Nested
    class DeleteOperations {
        @Test
        void testDeleteUsersByCommaSeparatedIds() {
            int updatedCount = userMapper.deleteUsersByCommaSeparatedIds("502, 503");
            Assertions.assertEquals(2, updatedCount);
        }
    }

    @Nested
    class CachingOperations {

        @Test
        void test1stCache() {
            User u1 = userMapper.getById(21);
            User u2 = userMapper.getById(21);
            System.out.println(u1 == u2);
        }
    }

    @Test
    void testGetAllMappedStatements() {
        Configuration configuration = sqlSession.getConfiguration();
        Collection<MappedStatement> mappedStatements = configuration.getMappedStatements();
        System.out.printf("总共有%d条MappedStatement%n", mappedStatements.size());
        mappedStatements.stream().map(MappedStatement::getId)
                .distinct()
                .sorted()
                .forEach(System.out::println);
    }
}
