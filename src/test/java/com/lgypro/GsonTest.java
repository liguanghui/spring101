package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class GsonTest {
    static Gson gson;

    @BeforeAll
    static void init() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Test
    void primitivesExamples() {
        // Serialization
        System.out.println(gson.toJson(1));
        System.out.println(gson.toJson("abcd"));
        System.out.println(gson.toJson(Long.valueOf(10)));
        System.out.println(gson.toJson(new int[]{1}));
        // Deserialization
        System.out.println("--------------------");
        System.out.println(gson.fromJson("1", int.class));
        System.out.println(gson.fromJson("1", Integer.class));
        System.out.println(gson.fromJson("false", Boolean.class));
        System.out.println(gson.fromJson("\"abc\"", String.class));
        System.out.println(Arrays.toString(gson.fromJson("[\"abc\"]", String[].class)));
    }

    @Test
    void arrayExamples() {
        int[] ints = {1, 2, 3, 4, 5};
        String[] strings = {"abc", "def", "ghi"};

        System.out.println(gson.toJson(ints));
        System.out.println(gson.toJson(strings));

        System.out.println(Arrays.toString(gson.fromJson("[1, 2, 3, 4, 5]", int[].class)));
    }

    @Test
    void collectionsExamples() {
        // Serialization
        System.out.println(gson.toJson(Arrays.asList(1, 2, 3, 4, 5)));
        System.out.println("--------------------");
        // Deserialization
        TypeToken<Collection<Integer>> collectionType = new TypeToken<>() {
        };
        Collection<Integer> integers = gson.fromJson("[1, 2, 3, 4, 5]", collectionType);
        System.out.println(integers);
    }

    @Test
    void mapsExamples() {
        // Serialization
        Map<String, String> stringMap = new LinkedHashMap<>();
        stringMap.put("key", "value");
        stringMap.put(null, "null-entry");
        System.out.println(stringMap);
        System.out.println(gson.toJson(stringMap));
        // Deserialization
        System.out.println("--------------------");
        Map<Integer, Integer> integerMap = Map.of(2, 4, 3, 6);
        System.out.println(gson.toJson(integerMap));

        TypeToken<Map<String, String>> mapTypeToken = new TypeToken<>() {
        };
        String json = """
                {
                    "key": "value"
                }
                """;
        System.out.println(json);
        Map<String, String> map = gson.fromJson(json, mapTypeToken);
        System.out.println(map);
    }
}
