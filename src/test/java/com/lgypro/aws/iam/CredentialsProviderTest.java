package com.lgypro.aws.iam;

import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.profiles.Profile;
import software.amazon.awssdk.profiles.ProfileFile;
import software.amazon.awssdk.profiles.ProfileFileLocation;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;

public class CredentialsProviderTest {
    @Test
    void testDefaultCredentialsProvider() {
        DefaultCredentialsProvider credentialsProvider = DefaultCredentialsProvider.create();
        AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
        System.out.printf("ACCESS_KEY_ID=%s%nSECRET_ACCESS_KEY=%s%n", awsCredentials.accessKeyId(), awsCredentials.secretAccessKey());
    }

    @Test
    void testAnonymousCredentialsProvider() {
        AnonymousCredentialsProvider credentialsProvider = AnonymousCredentialsProvider.create();
        AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
        System.out.printf("ACCESS_KEY_ID=%s%nSECRET_ACCESS_KEY=%s%n", awsCredentials.accessKeyId(), awsCredentials.secretAccessKey());
    }

    @Test
    void testCreateAwsBasicCredentials() {
        String accessKeyId = "AKIAVAXLPKUEPOHRTDUE";
        String secretAccessKey = "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";
        AwsCredentials awsCredentials = AwsBasicCredentials.create(accessKeyId, secretAccessKey);
        System.out.printf("ACCESS_KEY_ID=%s%nSECRET_ACCESS_KEY=%s%n", awsCredentials.accessKeyId(), awsCredentials.secretAccessKey());
    }

    @Test
    void testAwsCredentialsProviderChain() {
        AwsCredentialsProvider[] credentialsProviders = new AwsCredentialsProvider[]{
                SystemPropertyCredentialsProvider.create(),
                EnvironmentVariableCredentialsProvider.create(),
                WebIdentityTokenFileCredentialsProvider.create(),
                ProfileCredentialsProvider.create(),
                ContainerCredentialsProvider.builder().build(),
                InstanceProfileCredentialsProvider.create()
        };

        AwsCredentialsProvider credentialsProvider = AwsCredentialsProviderChain.builder()
                .reuseLastProviderEnabled(true)
                .credentialsProviders(credentialsProviders)
                .build();
        System.out.println(credentialsProvider);
        AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
        System.out.printf("ACCESS_KEY_ID=%s%nSECRET_ACCESS_KEY=%s%n", awsCredentials.accessKeyId(), awsCredentials.secretAccessKey());
    }

    @Test
    void testSystemPropertyCredentialsProvider() {
        SystemPropertyCredentialsProvider credentialsProvider = SystemPropertyCredentialsProvider.create();
        Assertions.assertThrows(SdkClientException.class, () -> {
            AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
            System.out.println(awsCredentials);
        });
    }

    @Test
    void testEnvironmentVariableCredentialsProvider() {
        EnvironmentVariableCredentialsProvider credentialsProvider = EnvironmentVariableCredentialsProvider.create();
        Assertions.assertThrowsExactly(SdkClientException.class, () -> {
            AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
            System.out.println(awsCredentials);
        });
    }

    @Test
    void testProfileCredentialsProvider() {
        ProfileCredentialsProvider credentialsProvider = ProfileCredentialsProvider.create();
        AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
        System.out.println(awsCredentials);
    }

    @Test
    void testProfileFileLocation() {
        Optional<Path> configurationFileLocation = ProfileFileLocation.configurationFileLocation();
        Optional<Path> credentialsFileLocation = ProfileFileLocation.credentialsFileLocation();
        Assertions.assertAll(
                () -> Assertions.assertTrue(configurationFileLocation.isPresent()),
                () -> Assertions.assertTrue(credentialsFileLocation.isPresent()),
                () -> Assertions.assertEquals(
                        new StringJoiner(File.separator)
                                .add(System.getenv("HOME"))
                                .add(".aws")
                                .add("config")
                                .toString(),
                        configurationFileLocation.get().toString()),
                () -> Assertions.assertEquals(
                        new StringJoiner(File.separator)
                                .add(System.getenv("HOME"))
                                .add(".aws")
                                .add("credentials")
                                .toString(),
                        credentialsFileLocation.get().toString())
        );
        configurationFileLocation.ifPresent(System.out::println);
        credentialsFileLocation.ifPresent(System.out::println);
    }

    @Test
    void testCredentialsProfileFile() {
        Optional<Path> credentialsFileLocation = ProfileFileLocation.credentialsFileLocation();
        Assertions.assertTrue(credentialsFileLocation.isPresent());
        ProfileFile credentialsProfileFile = ProfileFile
                .builder()
                .content(credentialsFileLocation.get())
                .type(ProfileFile.Type.CREDENTIALS)
                .build();
        System.out.println(credentialsProfileFile);
        MetaObject metaObject = new Configuration().newMetaObject(credentialsProfileFile);
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Profile>> profilesAndSectionsMap = (Map<String, Map<String, Profile>>) metaObject.getValue("profilesAndSectionsMap");
        System.out.println(profilesAndSectionsMap);
    }

    @Test
    void testConfigurationProfileFile() {
        Optional<Path> configurationFileLocation = ProfileFileLocation.configurationFileLocation();
        Assertions.assertTrue(configurationFileLocation.isPresent());
        ProfileFile configurationProfileFile = ProfileFile
                .builder()
                .content(configurationFileLocation.get())
                .type(ProfileFile.Type.CREDENTIALS)
                .build();
        System.out.println(configurationProfileFile);
        MetaObject metaObject = new Configuration().newMetaObject(configurationProfileFile);
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Profile>> profilesAndSectionsMap = (Map<String, Map<String, Profile>>) metaObject.getValue("profilesAndSectionsMap");
        System.out.println(profilesAndSectionsMap);
    }

    @Test
    void testDefaultProfileFile() {
        ProfileFile profileFile = ProfileFile.defaultProfileFile();
        System.out.println(profileFile);
        MetaObject metaObject = new Configuration().newMetaObject(profileFile);
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Profile>> profilesAndSectionsMap = (Map<String, Map<String, Profile>>) metaObject.getValue("profilesAndSectionsMap");
        System.out.println(profilesAndSectionsMap);
    }

    @Test
    void testInstanceProfileCredentialsProvider() {
        InstanceProfileCredentialsProvider credentialsProvider = InstanceProfileCredentialsProvider.create();
        System.out.println(credentialsProvider);
    }
}
