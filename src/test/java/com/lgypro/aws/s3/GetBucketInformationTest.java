package com.lgypro.aws.s3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

class GetBucketInformationTest {
    S3Client s3Client;

    @BeforeEach
    void init() {
        s3Client = S3Client.builder()
                .region(Region.AP_NORTHEAST_1)
                .credentialsProvider(ProfileCredentialsProvider.create("demo-role"))
                .build();
    }

    @AfterEach
    void destroy() {
        if (s3Client != null) {
            s3Client.close();
        }
    }

    @Test
    void testGetBucketAccelerateConfiguration() {
        String bucketName = "lgypro-mydemobucket";
        GetBucketInformation.getBucketAccelerateConfiguration(s3Client, bucketName);
    }

    @Test
    void testGetBucketAcl() {
        String bucketName = "lgypro-mydemobucket";
        GetBucketInformation.getBucketAcl(s3Client, bucketName);
    }

    @Test
    void testGetBucketOwnershipControls() {
        String bucketName = "lgypro-mydemobucket";
        String objectOwnership = GetBucketInformation.getBucketOwnershipControls(s3Client, bucketName);
        System.out.println(objectOwnership);
    }
}