package com.lgypro.aws;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.*;

import java.util.List;

public class ServiceClientInstantiationTest {
    private static Ec2Client ec2Client;

    @BeforeAll
    static void initAll() {
        ec2Client = DependencyFactory.ec2Client();

    }

    @AfterAll
    static void destroy() {
        ec2Client.close();
    }

    @Test
    void testEc2Client() {
        System.out.println(ec2Client);
    }

    @Test
    void testDescribeRegions() {
        DescribeRegionsResponse regionsResponse = ec2Client.describeRegions();
        regionsResponse.regions().forEach(region -> {
            System.out.printf("Found Region %s with endpoint %s%n", region.regionName(), region.endpoint());
        });
    }

    @Test
    void testDescribeAvailabilityZones() {
        DescribeAvailabilityZonesResponse zonesResponse = ec2Client.describeAvailabilityZones();
        List<AvailabilityZone> availabilityZones = zonesResponse.availabilityZones();
        availabilityZones.forEach(availabilityZone -> {
            System.out.printf("Found Availability Zone %s ('%s') with status %s in region %s%n",
                    availabilityZone.zoneName(),
                    availabilityZone.zoneId(),
                    availabilityZone.state(),
                    availabilityZone.regionName()
            );
        });
    }

    @Test
    void testDescribeAccountAttributes() {
        DescribeAccountAttributesResponse accountAttributes = ec2Client.describeAccountAttributes();
        accountAttributes.accountAttributes().forEach(accountAttribute -> {
            List<String> values = accountAttribute.attributeValues().stream().map(AccountAttributeValue::attributeValue).toList();
            System.out.printf("%s => %s%n", accountAttribute.attributeName(), values);
        });
    }
}
