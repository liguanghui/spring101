package com.lgypro;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public interface TestLifecycleLogger {
    Logger log = LoggerFactory.getLogger(TestLifecycleLogger.class);

    @BeforeAll
    static void beforeAllTests() {
        log.info("Before all tests");
    }

    @AfterAll
    static void afterAllTests() {
        log.info("After all tests");
    }

    @BeforeEach
    default void beforeEachTest(TestInfo testInfo) {
        log.info(String.format("About to execute [%s]", testInfo.getDisplayName()));
    }

    @AfterEach
    default void afterEachTest(TestInfo testInfo) {
        log.info(String.format("Finished executing [%s]", testInfo.getDisplayName()));
    }
}
