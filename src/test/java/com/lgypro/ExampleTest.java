package com.lgypro;

import com.lgypro.annotation.Fast;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Fast
public class ExampleTest {
    private static Logger log;

    @BeforeAll
    static void setUp() {
        System.out.println("set up logger instance");
        log = LoggerFactory.getLogger(ExampleTest.class);
    }

    public ExampleTest() {
        log.info("instantiate an ExampleTest instance");
    }

    @BeforeEach
    void init() {
        log.info("create a new instance");
    }

    @Test
    void givenMultipleAssertion() {
        assertAll("heading",
                () -> assertEquals(4, 2 * 2, "4 is 2 times 2"),
                () -> assertEquals("java", "JAVA".toLowerCase()),
                () -> assertEquals((Object) null, null, "null is equal to null"));
    }

    @Test
    @DisplayName("Arrays should be equals")
    public void whenAssertingArraysEquality_thenEqual() {
        char[] expected = {'J', 'u', 'p', 'i', 't', 'e', 'r'};
        char[] actual = "Jupiter".toCharArray();
        assertArrayEquals(expected, actual, "Arrays should be equal");
    }
}
