package com.lgypro;

import com.lgypro.entity.Department;
import com.lgypro.entity.Employee;
import com.lgypro.entity.User;
import com.lgypro.factory.LoggingObjectFactory;
import com.lgypro.mapper.DepartmentMapper;
import com.lgypro.mapper.EmployeeMapper;
import com.lgypro.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.property.PropertyTokenizer;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class MybatisRelatedToolsTest {
    private static SqlSessionFactory sqlSessionFactory;

    @BeforeAll
    static void initAll() throws IOException {
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
    }

    @Test
    void testTypeAliasRegistry() {
        Configuration configuration = sqlSessionFactory.getConfiguration();
        TypeAliasRegistry typeAliasRegistry = configuration.getTypeAliasRegistry();
        Map<String, Class<?>> typeAliases = typeAliasRegistry.getTypeAliases();
        System.out.printf("总共注册了%d个类型别名%n", typeAliases.size());
        typeAliases.forEach((name, clazz) -> {
            System.out.println(name + " -> " + clazz);
        });
    }

    @Test
    void testTypeHandlerRegistry() throws NoSuchFieldException, IllegalAccessException {
        Configuration configuration = sqlSessionFactory.getConfiguration();
        TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
        Field jdbcTypeHandlerMapField = TypeHandlerRegistry.class.getDeclaredField("jdbcTypeHandlerMap");
        jdbcTypeHandlerMapField.setAccessible(true);
        @SuppressWarnings("unchecked") Map<JdbcType, TypeHandler<?>> jdbcTypeTypeHandlerMap = (Map<JdbcType, TypeHandler<?>>) jdbcTypeHandlerMapField.get(typeHandlerRegistry);
        System.out.println("jdbcTypeHandlerMap: ");
        jdbcTypeTypeHandlerMap.forEach((jdbcType, typeHandler) -> {
            System.out.println(jdbcType + " -> " + typeHandler.getClass().getName() + "@" + Integer.toHexString(typeHandler.hashCode()));
        });
        Field typeHandlerMapField = TypeHandlerRegistry.class.getDeclaredField("typeHandlerMap");
        typeHandlerMapField.setAccessible(true);
        @SuppressWarnings("unchecked") Map<Type, Map<JdbcType, TypeHandler<?>>> typeHandlerMap = (Map<Type, Map<JdbcType, TypeHandler<?>>>) typeHandlerMapField.get(typeHandlerRegistry);
        System.out.println("typeHandlerMap: ");
        typeHandlerMap.forEach((type, typeMap) -> {
            System.out.println(type + " -> ");
            typeMap.forEach((jdbcType, typeHandler) -> {
                System.out.println("\t" + jdbcType + " -> " + typeHandler.getClass().getName() + "@" + Integer.toHexString(typeHandler.hashCode()));
            });
        });
    }

    @Test
    void testSecondLevelCacheRaw() {
        Cache cache = sqlSessionFactory.getConfiguration().getCache("com.lgypro.mapper.UserMapper");
        cache.putObject("name", "Welcome to China!");
        String name = (String) cache.getObject("name");
        System.out.println(cache);
        System.out.println(name);
    }

    @Test
    @DisplayName("check if 2nd level cache can be used in two distinct sqlSession instance")
    void testSecondLevelCacheIn2SqlSession() {
        Cache cache;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            User user = mapper.getById(27);
            System.out.println(user);
            mapper.getById(27);
            cache = sqlSession.getConfiguration().getCache(UserMapper.class.getName());
            System.out.println("before commit, size of cache = " + cache.getSize());
            sqlSession.commit();
        }
        System.out.println("after commit, size of cache = " + cache.getSize());
        System.out.println("--------------------");
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            User user = mapper.getById(27);
            System.out.println(user);
            mapper.getById(27);
        }
    }

    @Test
    void simpleUseMetaObject() {
        Employee e = null;
        Department d = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            e = mapper.findEmployeeExactlyByEmpNo(7369);
            System.out.println(e);
        }
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            DepartmentMapper mapper = sqlSession.getMapper(DepartmentMapper.class);
            d = mapper.findDepartmentAndEmployees(20);
            System.out.println(d);
        }
        Configuration configuration = sqlSessionFactory.getConfiguration();
        MetaObject metaObject = configuration.newMetaObject(e);
        System.out.println(metaObject);
        Object department = metaObject.getValue("department");
        System.out.println(department);
        metaObject.setValue("department", new Department(60, "IETF", "New York"));
        System.out.println(metaObject.getValue("department"));
        metaObject.setValue("department.location", "LG");
        System.out.println(metaObject.getValue("department"));
    }

    @Test
    void testSimplePropertyTokenizer() {
        PropertyTokenizer pt = new PropertyTokenizer("comments[0].author.name");
        System.out.printf("indexedName=(%s),name=(%s),index=(%s),children=(%s)%n", pt.getIndexedName(), pt.getName(), pt.getIndex(), pt.getChildren());
        while (pt.hasNext()) {
            pt = pt.next();
            System.out.printf("indexedName=(%s),name=(%s),index=(%s),children=(%s)%n", pt.getIndexedName(), pt.getName(), pt.getIndex(), pt.getChildren());
        }
    }

    @Nested
    class ObjectFactoryTest {

        ObjectFactory objectFactory;

        @BeforeEach
        void init() {
            objectFactory = new DefaultObjectFactory();
        }

        @Test
        void testIsCollection() {
            Assertions.assertTrue(objectFactory.isCollection(ArrayList.class));
        }

        @Test
        void testInstantiateWithDefaultConstructor() {
            @SuppressWarnings("unchecked")
            List<Object> list = objectFactory.create(List.class);
            Assertions.assertSame(ArrayList.class, list.getClass());
        }

        @Test
        void testInstantiateWithCustomizedConstructor() {
            Department department = objectFactory.create(Department.class,
                    List.of(int.class, String.class, String.class),
                    List.of(100, "混吃等死事业部", "油锅地狱"));
            Assertions.assertAll(
                    () -> Assertions.assertEquals(100, department.getId()),
                    () -> Assertions.assertEquals("混吃等死事业部", department.getName()),
                    () -> Assertions.assertNotEquals("油锅地域", department.getLocation())
            );
        }
    }

    @Test
    void testCustomizedObjectFactory() {
        Configuration configuration = sqlSessionFactory.getConfiguration();
        ObjectFactory objectFactory = configuration.getObjectFactory();
        Assertions.assertSame(LoggingObjectFactory.class, objectFactory.getClass());
    }
}
