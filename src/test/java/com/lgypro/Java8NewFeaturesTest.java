package com.lgypro;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Java8NewFeaturesTest {
    @Test
    void testTraditionalDate() {
        Date d1 = new Date();
        System.out.println(d1);
        Instant instant = d1.toInstant();
        System.out.println(instant);
    }

    @Test
    void testTraditionalSimpleDateFormat() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(formatter.format(new Date()));
        System.out.println(formatter.format(new Date(24 * 60 * 60 * 1000 + 40 * 60 * 1000)));
        Date date = formatter.parse("1970-01-01 09:40:00");
        System.out.println(date);
        System.out.println(date.getTime());
    }

    @Test
    void testCalendar() {

    }

    @Test
    void testLocalDateTime() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        LocalDateTime d1 = LocalDateTime.of(2015, 3, 20, 10, 8, 7);
        System.out.println(d1);

        LocalDateTime d2 = now.plusYears(2);
        System.out.println(d2);

        LocalDateTime d3 = now.minusMonths(3);
        System.out.println(d3);
    }

    @Test
    void testInstant() {
        Instant now = Instant.now();
        System.out.println(now);
        System.out.println(now.toEpochMilli());
    }

    @Test
    void testDuration() {
        Instant i1 = Instant.now();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Instant i2 = Instant.now();
        Duration d1 = Duration.between(i1, i2);
        System.out.println(d1.toMillis());

    }
}
