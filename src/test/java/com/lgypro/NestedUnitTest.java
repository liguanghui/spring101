package com.lgypro;

import com.lgypro.annotation.Fast;
import com.lgypro.annotation.FastTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.EmptyStackException;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class NestedUnitTest {
    NestedUnitTest() {
        System.out.println("thread is " + Thread.currentThread().getName());
        System.out.println("create a NestedUnitTest instance = " + this);
    }

    Stack<Object> stack;

    @BeforeAll
    static void beforeAll() {
        System.out.println("NestedUnitTest#beforeAll");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("NestedUnitTest#afterAll");
    }

    @BeforeEach
    void init() {
        System.out.println(this + " " + "#beforeEach");
    }

    @AfterEach
    void destroy() {
        System.out.println(this + " " + "#afterEach");
        System.out.println("########################################");
    }


    @FastTest
    @DisplayName("is instantiated with new Stack()")
    void isInstantiatedWithNew() {
        System.out.println("1");
        new Stack<>();
    }

    @Nested
    @DisplayName("when new")
    class WhenNew {
        WhenNew() {
            System.out.println("2");
            System.out.println("create a whenNew instance = " + this);
        }

        @BeforeEach
        void init() {
            System.out.println("3");
            System.out.println(this + " " + "#beforeEach");
            stack = new Stack<>();
        }

        @AfterEach
        void destroy() {
            System.out.println(this + " " + "#afterEach");
        }

        @Test
        @DisplayName("is empty")
        void isEmpty() {
            System.out.println("4");
            assertTrue(stack.isEmpty());
        }

        @Test
        @DisplayName("throws EmptyStackException when popped")
        void throwsExceptionWhenPopped() {
            System.out.println("5");
            assertThrows(EmptyStackException.class, stack::pop);
        }

        @Test
        @DisplayName("throws EmptyStackException when peeked")
        void throwsExceptionWhenPeeked() {
            System.out.println("6");
            assertThrows(EmptyStackException.class, stack::peek);
        }

        @Nested
        @DisplayName("after pushing an element")
        class AfterPushing {
            AfterPushing() {
                System.out.println("7");
                System.out.println("create an afterPushing instance = " + this);
            }

            String anElement = "an element";

            @BeforeEach
            void init() {
                System.out.println("8");
                System.out.println(this + " " + "#beforeEach");
                stack.push(anElement);
            }

            @AfterEach
            void destroy() {
                System.out.println(this + " #afterEach");
            }

            @Test
            @DisplayName("it is no longer empty")
            void isEmpty() {
                System.out.println("9");
                assertFalse(stack.isEmpty());
            }

            @Test
            @DisplayName("returns the element when popped and is empty")
            void returnElementWhenPopped() {
                System.out.println("10");
                assertEquals(anElement, stack.pop());
                assertTrue(stack.isEmpty());
            }

            @FastTest
            @DisplayName("returns the element when peeked but remains not empty")
            void returnElementWhenPeeked() {
                System.out.println("11");
                assertEquals(anElement, stack.peek());
                assertFalse(stack.isEmpty());
            }
        }
    }
}
