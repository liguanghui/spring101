package com.lgypro;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.druid.sql.ast.statement.SQLColumnUniqueKey;
import com.lgypro.entity.User;
import org.apache.ibatis.executor.BatchExecutor;
import org.apache.ibatis.executor.CachingExecutor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.ReuseExecutor;
import org.apache.ibatis.executor.SimpleExecutor;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ExecutorTest {
    private static SqlSessionFactory sqlSessionFactory;
    private static Configuration configuration;
    private static DruidDataSource dataSource;


    private static String driverClassName;
    private static String url;
    private static String username;
    private static String password;

    @BeforeAll
    static void init() throws IOException {
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
        configuration = sqlSessionFactory.getConfiguration();
        try (InputStream in = ExecutorTest.class.getClassLoader().getResourceAsStream("jdbc.properties")) {
            Properties properties = new Properties();
            properties.load(in);
            driverClassName = properties.getProperty("jdbc.driverClassName");
            url = properties.getProperty("jdbc.url");
            username = properties.getProperty("jdbc.username");
            password = properties.getProperty("jdbc.password");
        }
        dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
    }

    @Test
    void testSimpleExecutor() throws SQLException {
        JdbcTransaction tx = new JdbcTransaction(dataSource.getConnection());
        SimpleExecutor executor = new SimpleExecutor(configuration, tx);

        MappedStatement statement = configuration.getMappedStatement("com.lgypro.mapper.UserMapper.getById");

        List<User> userList = executor.doQuery(statement, 27, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER, statement.getBoundSql(27));
        System.out.println(userList.get(0));
        List<User> userList2 = executor.doQuery(statement, 31, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER, statement.getBoundSql(31));
        System.out.println(userList2.get(0));
    }

    @Test
    void testReuseExecutor() throws SQLException {
        JdbcTransaction tx = new JdbcTransaction(dataSource.getConnection());
        ReuseExecutor executor = new ReuseExecutor(configuration, tx);

        MappedStatement statement = configuration.getMappedStatement("com.lgypro.mapper.UserMapper.getById");

        List<User> userList = executor.doQuery(statement, 27, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER, statement.getBoundSql(27));
        System.out.println(userList.get(0));
        List<User> userList2 = executor.doQuery(statement, 31, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER, statement.getBoundSql(31));
        System.out.println(userList2.get(0));
    }

    @Test
    void testBatchExecutor() throws SQLException {
        JdbcTransaction tx = new JdbcTransaction(dataSource.getConnection());
        BatchExecutor executor = new BatchExecutor(configuration, tx);

        MappedStatement statement = configuration.getMappedStatement("com.lgypro.mapper.UserMapper.insertUser");
        User user = new User("鲁班", "ABC", '女', "luban@example.org");
        executor.doUpdate(statement, user);
        user = new User("鲁班2", "ABCD", '男', "luban2@example.org");
        executor.doUpdate(statement, user);
        executor.doFlushStatements(false);
    }

    @Test
    void testCachingExecutor() throws SQLException {
        JdbcTransaction tx = new JdbcTransaction(dataSource.getConnection());
        Executor executor = new CachingExecutor(new SimpleExecutor(configuration, tx));

        MappedStatement statement = configuration.getMappedStatement("com.lgypro.mapper.UserMapper.getById");

        List<User> userList = executor.query(statement, 27, RowBounds.DEFAULT, Executor.NO_RESULT_HANDLER);
        executor.commit(true);
        System.out.println(userList.get(0));
        List<User> userList2 = executor.query(statement, 27, RowBounds.DEFAULT, SimpleExecutor.NO_RESULT_HANDLER);
        System.out.println(userList2.get(0));
    }

    @Test
    void testSqlSession() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(true)) {
            List<User> userList = sqlSession.selectList("com.lgypro.mapper.UserMapper.getById", 30);
            System.out.println(userList.get(0));
            System.out.println(userList.get(0));
        }
    }

    @Test
    void testRawUseOfJDBC() throws ClassNotFoundException, SQLException {
        Class.forName(driverClassName);
        Connection connection = DriverManager.getConnection(url, username, password);
        String sql = "select * from users where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, 27);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            System.out.println(resultSet.getString("username"));
        }
    }

    @Test
    void testStatementBatchExecution() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            String sql1 = "insert into users (username, email) values ('alice', 'alice@example.org')";
            String sql2 = "insert into users (username, email) values ('bob', 'bob@example.org')";
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            int loopCount = 100;
            try {
                System.out.println("1. 测试批处理执行耗时");
                long startTime = System.nanoTime();
                for (int i = 0; i < loopCount; i++) {
                    statement.addBatch(sql1);
                }
                int[] updateCounts = statement.executeBatch();
                connection.commit();
                System.out.println("sum = " + Arrays.stream(updateCounts).sum());
                System.out.println("批处理执行耗时 " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + "ms");
            } catch (Exception e) {
                connection.rollback();
                throw e;
            }
            System.out.println("2. 测试单个执行耗时");
            try {
                long startTime = System.nanoTime();
                for (int i = 0; i < loopCount; i++) {
                    statement.executeUpdate(sql2);
                }
                connection.commit();
                System.out.println("单个执行耗时" + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + "ms");
            } catch (Exception e) {
                connection.rollback();
                throw e;
            }
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Test
    void testPreparedStatementBatchExecution() throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "insert into users (username, email) values (?, ?)";
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            int loopCount = 100;
            try {
                System.out.println("1. 测试批处理执行耗时");
                long startTime = System.nanoTime();
                for (int i = 0; i < loopCount; i++) {
                    preparedStatement.setString(1, "bob");
                    preparedStatement.setString(2, "bob@example.org");
                    preparedStatement.addBatch();
                }
                int[] updateCounts = preparedStatement.executeBatch();
                connection.commit();
                System.out.println("sum = " + Arrays.stream(updateCounts).sum());
                System.out.println("批处理执行耗时 " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + "ms");
            } catch (Exception e) {
                connection.rollback();
                throw e;
            }
            System.out.println("2. 测试单个执行耗时");
            try {
                long startTime = System.nanoTime();
                for (int i = 0; i < loopCount; i++) {
                    preparedStatement.setString(1, "alice");
                    preparedStatement.setString(2, "alice@example.com");
                    preparedStatement.executeUpdate();
                }
                connection.commit();
                System.out.println("单个执行耗时" + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + "ms");
            } catch (Exception e) {
                connection.rollback();
                throw e;
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Test
    void testConnectionAutoCommitStatus() throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            System.out.println("new connection auto-commit mode: " + connection.getAutoCommit());
        }
    }

    @Test
    void isDriverSupportsBatchUpdates() throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData dataBaseMetaData = connection.getMetaData();
            boolean supportsBatchUpdates = dataBaseMetaData.supportsBatchUpdates();
            System.out.println("支持批量更新吗？" + supportsBatchUpdates);
        }
    }

    @Test
    void isGlobalCacheEnabled() {
        boolean cacheEnabled = configuration.isCacheEnabled();
        Assertions.assertTrue(cacheEnabled);
    }

    @Test
    void testColumnLabelsWhenJoin2Tables() throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            String sql = "select * from emp left outer join dept on emp.deptno = dept.deptno where emp.empno = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, 7369);
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            List<String> columnLabels = new ArrayList<>();
            for (int i = 1; i <= columnCount; i++) {
                String columnLabel = metaData.getColumnLabel(i);
                columnLabels.add(columnLabel);
            }
            System.out.println(columnLabels);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @AfterAll
    static void destroy() {
        dataSource.close();
    }
}
