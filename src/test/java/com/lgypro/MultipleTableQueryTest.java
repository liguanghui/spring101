package com.lgypro;

import com.lgypro.entity.Department;
import com.lgypro.entity.Employee;
import com.lgypro.entity.SalaryGrade;
import com.lgypro.mapper.DepartmentMapper;
import com.lgypro.mapper.EmployeeMapper;
import com.lgypro.mapper.SalaryGradeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Pattern;

public class MultipleTableQueryTest {
    private static SqlSessionFactory sqlSessionFactory = null;
    private SqlSession sqlSession = null;

    @BeforeAll
    static void initAll() throws IOException {
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
    }

    @BeforeEach
    void initEach() {
        sqlSession = sqlSessionFactory.openSession(true);
    }

    @Test
    @DisplayName("whether configuration is correct?")
    void isEnvironmentReady() {

    }

    @Nested
    @DisplayName("Test Department Related Operations")
    class DepartmentTest {
        private DepartmentMapper departmentMapper;

        @BeforeEach
        void init() {
            departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
        }

        @Nested
        class QueryOperation {
            @Test
            void testFindExactlyById() {
                Department d1 = departmentMapper.findExactlyById(10);
                Department d2 = new Department(10, "ACCOUNTING", "NEW YORK");
                System.out.println(d1);
                Assertions.assertEquals(d2, d1);
            }

            @Test
            void testFindDepartmentAndEmployees() {
                Department department = departmentMapper.findDepartmentAndEmployees(10);
                System.out.println(department);
            }
        }
    }

    @Nested
    @DisplayName("Test Employee Related Operations")
    class EmployeeTest {
        private EmployeeMapper employeeMapper = null;

        @BeforeEach
        void init() {
            employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
        }

        @Nested
        class QueryOperations {
            @Test
            void testGetAllEmployees() {
                List<Employee> employeeList = employeeMapper.getAllEmployees();
                employeeList.forEach(System.out::println);
                sqlSession.commit();
            }

            @Test
            void testFindExactlyByEmpNo() {
                Employee e1 = employeeMapper.findEmployeeExactlyByEmpNo(7499);
                Employee e2 = new Employee(7499,
                        "ALLEN",
                        "SALESMAN",
                        7698,
                        LocalDate.parse("1981-02-20", DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                        1600,
                        300,
                        new Department(30, "SALES", "CHICAGO"));
                System.out.println(e1);
                Assertions.assertEquals(e2, e1);
                sqlSession.commit();
                employeeMapper.findEmployeeExactlyByEmpNo(7499);
            }

            @Test
            void testFindEmployeesByNameUseLikeOperator() {
                List<Employee> employeeList = employeeMapper.findEmployeesByNameUsingLikeOperator("AR");
                System.out.println(employeeList);
                Assertions.assertTrue(employeeList.stream()
                        .map(Employee::getName)
                        .allMatch(name -> Pattern.matches(".*AR.*", name)));
            }

            @Test
            void testFindEmployeesByNameUseRegularExpression() {
                List<Employee> employeeList = employeeMapper.findEmployeesByNameUsingRegularExpression(".*AR.*");
                System.out.println(employeeList);
                Assertions.assertTrue(employeeList.stream()
                        .map(Employee::getName)
                        .allMatch(name -> Pattern.matches(".*AR.*", name)));
            }
        }
    }

    @Nested
    @DisplayName("Test SalaryGrade Related Operations")
    class SalaryGradeTest {
        private SalaryGradeMapper salaryGradeMapper;

        @BeforeEach
        void init() {
            salaryGradeMapper = sqlSession.getMapper(SalaryGradeMapper.class);
        }

        @Test
        void testGetGradeFromSalary() {
            SalaryGrade grade = salaryGradeMapper.getGradeFromSalary(2500);
            System.out.println(grade);
            Assertions.assertEquals(4, grade.getGrade());
        }

    }

    @AfterEach
    void destroyEach() {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
