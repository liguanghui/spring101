package com.lgypro;

import com.lgypro.extension.RandomParametersExtension;
import com.lgypro.extension.RandomParametersExtension.Random;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Slf4j
@ExtendWith(RandomParametersExtension.class)
public class MyRandomParametersTest {
    @Test
    void injectsInteger(@Random int i, @Random int j) {
        log.info("i = {}, j = {}", i, j);
        assertNotEquals(i, j);
    }

    @Test
    void injectsDouble(@Random double d) {
        log.info("d = {}", d);
        assertEquals(0, d, 1.0);
    }
}
