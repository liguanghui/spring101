package com.lgypro;

import com.lgypro.annotation.FastTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class StandardTests {
    public StandardTests() {
        System.out.println("instantiate a StandardTests instance");
    }

    @BeforeAll
    static void initAll() {
        System.out.println("init all");
    }

    @BeforeEach
    void init() {
        System.out.println("init");
    }

    @Test
    void succeedingTest() {
        System.out.println("succeeding test");
    }

    @Test
    void failingTest() {
        // fail("a failing test");
    }

    @FastTest
    @Disabled("for demonstration purposes")
    void skippedTest() {
        // not executed
    }

    @Test
    void abortedTest() {
        assumeTrue("abc".contains("Z"));
        fail("test should have been aborted");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tear down");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("tear down all");
    }
}
