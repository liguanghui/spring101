import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;

import javax.annotation.Resource;

public class AutowiredAnnotationBeanPostProcessorTest {
    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.registerSingleton("bean2", new Bean2());
        beanFactory.registerSingleton("bean3", new Bean3());
        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());

        AutowiredAnnotationBeanPostProcessor autowiredAnnotationBeanPostProcessor = new AutowiredAnnotationBeanPostProcessor();
        autowiredAnnotationBeanPostProcessor.setBeanFactory(beanFactory);
        Bean1 bean1 = new Bean1();
        System.out.println(bean1);
    }

    @Slf4j
    static class Bean1 {
        private String home;
        private Bean2 bean2;
        private Bean3 bean3;

        @Autowired
        public void setHome(@Value("${JAVA_HOME}") String home) {
            log.debug("@Value does work {}", home);
            this.home = home;
        }

        @Autowired
        public void setBean2(Bean2 bean2) {
            log.debug("@Autowired does work {}", bean2);
            this.bean2 = bean2;
        }

        @Resource
        public void setBean3(Bean3 bean3) {
            log.debug("@Resource does work {}", bean3);
            this.bean3 = bean3;
        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "home='" + home + '\'' +
                    ", bean2=" + bean2 +
                    ", bean3=" + bean3 +
                    '}';
        }
    }

    static class Bean2 {

    }

    static class Bean3 {

    }
}
