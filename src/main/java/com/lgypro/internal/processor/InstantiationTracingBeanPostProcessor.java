package com.lgypro.internal.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

@Slf4j
public class InstantiationTracingBeanPostProcessor implements BeanPostProcessor, PriorityOrdered {
    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (log.isDebugEnabled()) {
            log.debug("Bean '" + beanName + "' created : " + bean.getClass().getName() + "@" + Integer.toHexString(bean.hashCode()));
        }
        return bean;
    }
}
