package com.lgypro.internal;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

/**
 * 设置ApplicationContext的applicationStartup使用自定义的带日志功能的实现后，发现BeanFactory还是使用默认的，特意写了这个BeanFactoryPostProcessor，为了记录更多的日志，这个后置处理器的执行优先级要高
 */
public class ReplacingBeanFactoryApplicationStartupImplementationWithApplicationContext implements BeanDefinitionRegistryPostProcessor, PriorityOrdered, ApplicationContextAware {
    private ApplicationContext applicationContext;

    public ReplacingBeanFactoryApplicationStartupImplementationWithApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public ReplacingBeanFactoryApplicationStartupImplementationWithApplicationContext() {
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        if (registry instanceof DefaultListableBeanFactory beanFactory) {
            if (applicationContext instanceof ConfigurableApplicationContext) {
                beanFactory.setApplicationStartup(((ConfigurableApplicationContext) applicationContext).getApplicationStartup());
            }
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
