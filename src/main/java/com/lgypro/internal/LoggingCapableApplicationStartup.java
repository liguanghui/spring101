package com.lgypro.internal;

import com.lgypro.internal.annotation.ReimplementedInfrastructure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.metrics.ApplicationStartup;
import org.springframework.core.metrics.StartupStep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * 替代ApplicationContext的默认实现类，在ApplicationContext启动的各个阶段（StartupStep）记录日志，方便理清ApplicationContext各启动阶段的启动时机
 */
@ReimplementedInfrastructure
public class LoggingCapableApplicationStartup implements ApplicationStartup {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCapableApplicationStartup.class);
    private static long nextId = 1L;
    private StartupStep currentStep;

    @Override
    public StartupStep start(String name) {
        currentStep = new LoggingCapableStartupStep(name, nextId++, currentStep, LOGGER);
        return currentStep;
    }

    static class LoggingCapableStartupStep implements StartupStep {
        private final Logger logger;
        private final String name;
        private final long id;
        private final StartupStep parentStep;
        private final DefaultTags tags = new DefaultTags();

        public LoggingCapableStartupStep(String name, long id, StartupStep parentStep, Logger logger) {
            this.name = name;
            this.id = id;
            this.parentStep = parentStep;
            this.logger = logger;
            if (logger.isDebugEnabled()) {
                logger.debug(this + " start");
            }
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public long getId() {
            return id;
        }

        @Override
        public Long getParentId() {
            if (parentStep != null) {
                return parentStep.getId();
            }
            return 0L;
        }

        @Override
        public StartupStep tag(String key, String value) {
            tags.addTag(new DefaultTag(key, value));
            return this;
        }

        @Override
        public StartupStep tag(String key, Supplier<String> value) {
            tags.addTag(new DefaultTag(key, value.get()));
            return this;
        }

        @Override
        public Tags getTags() {
            return tags;
        }

        @Override
        public void end() {
            if (logger.isDebugEnabled()) {
                logger.debug(this + " end");
            }
        }

        @Override
        public String toString() {
            return new StringBuilder()
                    .append("StartupStep {")
                    .append("name: ").append(name)
                    .append(", id: ").append(id)
                    .append(", tags: ").append(tags)
                    .append("}")
                    .toString();
        }

        static class DefaultTag implements StartupStep.Tag {
            private final String key;
            private final String value;

            public DefaultTag(String key, String value) {
                Objects.requireNonNull(key);
                this.key = key;
                this.value = value;
            }

            @Override
            public String getKey() {
                return key;
            }

            @Override
            public String getValue() {
                return value;
            }

            @Override
            public String toString() {
                return key + "=" + value;
            }
        }

        static class DefaultTags implements StartupStep.Tags {
            private List<Tag> tagList = new ArrayList<>();

            public void addTag(Tag tag) {
                tagList.add(tag);
            }

            @Override
            public Iterator<Tag> iterator() {
                return tagList.iterator();
            }

            @Override
            public String toString() {
                return String.valueOf(tagList);
            }
        }
    }
}
