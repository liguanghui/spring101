package com.lgypro.factory;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;


@Slf4j
public class LoggingObjectFactory extends DefaultObjectFactory {
    Properties properties;

    @Override
    public <T> T create(Class<T> type) {
        return super.create(type);
    }

    @Override
    public <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs) {
        if ("true".equals(properties.getProperty("objectFactoryLoggingEnabled"))) {
            if (constructorArgTypes == null && constructorArgs == null) {
                log.debug("creating {}", type.getName());
            } else {
                log.debug("creating {}, argTypes: {}, args: {}", type.getName(),
                        Optional.ofNullable(constructorArgTypes)
                                .orElseGet(Collections::emptyList)
                                .stream()
                                .map(Class::getSimpleName)
                                .collect(Collectors.joining(",")),
                        Optional.ofNullable(constructorArgs)
                                .orElseGet(Collections::emptyList)
                                .stream()
                                .map(String::valueOf)
                                .collect(Collectors.joining(",")));
            }
        }
        return super.create(type, constructorArgTypes, constructorArgs);
    }

    @Override
    protected Class<?> resolveInterface(Class<?> type) {
        return super.resolveInterface(type);
    }

    @Override
    public <T> boolean isCollection(Class<T> type) {
        return super.isCollection(type);
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
