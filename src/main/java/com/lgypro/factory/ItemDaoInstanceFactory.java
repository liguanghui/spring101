package com.lgypro.factory;

import com.lgypro.dao.ItemDao;
import com.lgypro.dao.impl.ItemDaoImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ItemDaoInstanceFactory {
    public ItemDaoInstanceFactory() {
        log.debug("Constructing ItemDaoInstanceFactory instance");
    }

    public ItemDao itemDao() {
        log.debug("Constructing ItemDao instance with instance factory method");
        return new ItemDaoImpl();
    }
}
