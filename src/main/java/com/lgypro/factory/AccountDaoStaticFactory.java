package com.lgypro.factory;

import com.lgypro.dao.AccountDao;
import com.lgypro.dao.impl.AccountDaoImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AccountDaoStaticFactory {
    public static AccountDao accountDao() {
        log.debug("Constructing AccountDao instance with static factory method");
        return new AccountDaoImpl();
    }
}
