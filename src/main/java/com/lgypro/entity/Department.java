package com.lgypro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
public class Department {
    private int id;
    private String name;
    private String location;
    private List<Employee> employees;

    public Department() {
    }

    public Department(int id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }
}
