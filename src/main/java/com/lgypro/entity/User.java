package com.lgypro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int id;
    private String username;
    private String password;
    private char gender;
    private String email;

    public User(String username, String password, char gender, String email) {
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.email = email;
    }
}
