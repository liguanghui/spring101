package com.lgypro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private int id;
    private String name;
    private String job;
    private int managerId;

    private LocalDate hireDate;

    private double salary;
    private double commission;

    private Department department;
}
