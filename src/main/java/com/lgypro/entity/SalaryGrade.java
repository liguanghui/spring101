package com.lgypro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalaryGrade {
    private int grade;
    private int lowSalary;
    private int highSalary;
}
