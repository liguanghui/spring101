package com.lgypro.aws;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.s3.S3Client;

public class DependencyFactory {

    static Region DEFAULT_REGION = Region.AP_NORTHEAST_1;

    private DependencyFactory() {
    }

    /**
     * @return an instance of S3Client
     */
    public static S3Client s3Client() {
        return S3Client.builder()
                .region(DEFAULT_REGION)
                .httpClientBuilder(ApacheHttpClient.builder())
                .build();
    }

    public static S3Client s3Client(Region region, String profileName) {
        return S3Client.builder()
                .region(region)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
    }

    public static Ec2Client ec2Client() {
        return Ec2Client.builder()
                .region(DEFAULT_REGION)
                .build();
    }

    public static IamClient iamClient() {
        return IamClient.builder()
                .region(Region.AWS_GLOBAL)
                .credentialsProvider(ProfileCredentialsProvider.create())
                .build();
    }

    public static IamClient iamClient(String profileName) {
        return IamClient.builder()
                .region(Region.AWS_GLOBAL)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
    }
}
