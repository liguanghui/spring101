package com.lgypro.aws.iam;

import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.*;
import software.amazon.awssdk.services.iam.waiters.IamWaiter;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.services.sts.StsClient;
import software.amazon.awssdk.services.sts.model.AssumeRoleRequest;
import software.amazon.awssdk.services.sts.model.AssumeRoleResponse;
import software.amazon.awssdk.services.sts.model.Credentials;
import software.amazon.awssdk.services.sts.model.StsException;

import java.util.List;
import java.util.concurrent.TimeUnit;

/*
1. 创建一个IAM用户scott
2. 获取到scott用户的ak/sk
3. 创建一个策略TrainBucketReadOnly
4. 创建一个角色train-role
5. 把TrainBucketReadOnly策略绑定到train-role上
6. 通过scott用户的ak/sk向sts服务发起请求，担任train-role，获取临时凭据，再使用临时凭据列出lgypro-train里的所有对象
 */
public class IAMScenario {
    public static final String DASHES = "--------------------------------------------------------------------------------";
    public static final String PolicyDocument = """
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "s3:*"
                        ],
                        "Resource": [
                            "arn:aws:s3:::lgypro-train",
                            "arn:aws:s3:::lgypro-train/*"
                        ]
                    }
                ]
            }
            """;
    public static String userArn;

    public static void main(String[] args) throws Exception {

        String userName = "scott";
        String policyName = "TrainBucketReadOnly";
        String roleName = "train-role";
        String roleSessionName = "train-session";
        String bucketName = "lgypro-train";

        Region region = Region.AWS_GLOBAL;
        IamClient iamClient = IamClient.builder()
                .region(region)
                .credentialsProvider(ProfileCredentialsProvider.create("default"))
                .build();

        System.out.println(DASHES);
        System.out.println("Welcome to the AWS IAM example scenario.");
        System.out.println(DASHES);

        System.out.println(DASHES);
        System.out.println(" 1. Create the IAM user.");
        User user = createIAMUser(iamClient, userName);

        System.out.println(DASHES);
        userArn = user.arn();

        AccessKey myKey = createIAMAccessKey(iamClient, userName);
        String accessKey = myKey.accessKeyId();
        String secretKey = myKey.secretAccessKey();
        String assumeRolePolicyDocument = String.format(
                """
                        {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "sts:AssumeRole"
                                    ],
                                    "Principal": {
                                        "AWS": "%s"
                                    }
                                }
                            ]
                        }
                        """,
                userArn);

        System.out.println(assumeRolePolicyDocument);
        System.out.println(userName + " was successfully created.");
        System.out.println(DASHES);
        System.out.println("2. Creates a policy.");
        String policyArn = createIAMPolicy(iamClient, policyName);
        System.out.println("The policy " + policyArn + " was successfully created.");
        System.out.println(DASHES);

        System.out.println(DASHES);
        System.out.println("3. Creates a role.");
        TimeUnit.SECONDS.sleep(30);
        String roleArn = createIAMRole(iamClient, roleName, assumeRolePolicyDocument);
        System.out.println(roleArn + " was successfully created.");
        System.out.println(DASHES);

        System.out.println(DASHES);
        System.out.println("4. Grants the role permissions.");
        attachIAMRolePolicy(iamClient, roleName, policyArn);
        System.out.println(DASHES);

        System.out.println(DASHES);
        System.out.println("*** Wait for 30 secs so the resource is available");
        TimeUnit.SECONDS.sleep(30);
        System.out.println("5. Gets temporary credentials by assuming the role.");
        System.out.println("Perform an Amazon S3 Service operation using the temporary credentials.");
        assumeRole(roleArn, roleSessionName, bucketName, accessKey, secretKey);
        System.out.println(DASHES);

        System.out.println("This IAM Scenario has successfully completed");
        System.out.println(DASHES);
    }

    public static AccessKey createIAMAccessKey(IamClient iamClient, String user) {
        try {
            CreateAccessKeyRequest request = CreateAccessKeyRequest.builder()
                    .userName(user)
                    .build();
            CreateAccessKeyResponse response = iamClient.createAccessKey(request);
            return response.accessKey();
        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return null;
    }

    public static User createIAMUser(IamClient iamClient, String username) {
        try {
            // Create an IamWaiter object
            IamWaiter iamWaiter = iamClient.waiter();
            CreateUserRequest request = CreateUserRequest.builder()
                    .userName(username)
                    .build();

            // Wait until the user is created.
            CreateUserResponse response = iamClient.createUser(request);
            GetUserRequest userRequest = GetUserRequest.builder()
                    .userName(response.user().userName())
                    .build();

            WaiterResponse<GetUserResponse> waitUntilUserExists = iamWaiter.waitUntilUserExists(userRequest);
            waitUntilUserExists.matched().response().ifPresent(System.out::println);
            return response.user();
        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return null;
    }

    public static String createIAMRole(IamClient iamClient, String roleName, String json) {
        try {
            CreateRoleRequest request = CreateRoleRequest.builder()
                    .roleName(roleName)
                    .assumeRolePolicyDocument(json)
                    .description("Created using the AWS SDK for Java")
                    .build();

            CreateRoleResponse response = iamClient.createRole(request);
            System.out.println("The ARN of the role is " + response.role().arn());
            return response.role().arn();
        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return "";
    }

    public static String createIAMPolicy(IamClient iamClient, String policyName) {
        try {
            // Create an IamWaiter object.
            IamWaiter iamWaiter = iamClient.waiter();
            CreatePolicyRequest request = CreatePolicyRequest.builder()
                    .policyName(policyName)
                    .policyDocument(PolicyDocument).build();

            CreatePolicyResponse response = iamClient.createPolicy(request);
            GetPolicyRequest polRequest = GetPolicyRequest.builder()
                    .policyArn(response.policy().arn())
                    .build();

            WaiterResponse<GetPolicyResponse> waitUntilPolicyExists = iamWaiter.waitUntilPolicyExists(polRequest);
            waitUntilPolicyExists.matched().response().ifPresent(System.out::println);
            return response.policy().arn();
        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return "";
    }

    public static void attachIAMRolePolicy(IamClient iamClient, String roleName, String policyArn) {
        try {
            ListAttachedRolePoliciesRequest request = ListAttachedRolePoliciesRequest.builder()
                    .roleName(roleName)
                    .build();

            ListAttachedRolePoliciesResponse response = iamClient.listAttachedRolePolicies(request);
            List<AttachedPolicy> attachedPolicies = response.attachedPolicies();
            String polArn;
            for (AttachedPolicy policy : attachedPolicies) {
                polArn = policy.policyArn();
                if (policyArn != null && policyArn.equals(polArn)) {
                    System.out.println(roleName + " policy is already attached to this role.");
                    return;
                }
            }

            AttachRolePolicyRequest attachRequest = AttachRolePolicyRequest.builder()
                    .roleName(roleName)
                    .policyArn(policyArn)
                    .build();

            iamClient.attachRolePolicy(attachRequest);
            System.out.println("Successfully attached policy " + policyArn + " to role " + roleName);
        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }

    // Invoke an Amazon S3 operation using the Assumed Role.
    // snippet-start:[iam.java2.scenario.assumeRole]
    public static void assumeRole(String roleArn, String roleSessionName, String bucketName, String accessKeyId, String secretAccessKey) {

        // Use the credentials of the new IAM user that was created in this code example.
        AwsCredentials credentials = AwsBasicCredentials.create(accessKeyId, secretAccessKey);
        StsClient stsClient = null;
        S3Client s3Client = null;
        try {
            stsClient = StsClient.builder()
                    .region(Region.AP_NORTHEAST_2)
                    .credentialsProvider(StaticCredentialsProvider.create(credentials))
                    .build();
            AssumeRoleRequest roleRequest = AssumeRoleRequest.builder()
                    .roleArn(roleArn)
                    .roleSessionName(roleSessionName)
                    .build();

            AssumeRoleResponse roleResponse = stsClient.assumeRole(roleRequest);
            Credentials roleSessionCredentials = roleResponse.credentials();
            String roleSessionAccessKeyId = roleSessionCredentials.accessKeyId();
            String roleSessionSecretAccessKey = roleSessionCredentials.secretAccessKey();
            String roleSessionToken = roleSessionCredentials.sessionToken();

            // List all objects in an Amazon S3 bucket using the temp credentials retrieved by invoking assumeRole.
            Region region = Region.AP_NORTHEAST_2;
            s3Client = S3Client.builder()
                    .credentialsProvider(StaticCredentialsProvider.create(
                            AwsSessionCredentials.create(roleSessionAccessKeyId,
                                    roleSessionSecretAccessKey,
                                    roleSessionToken)))
                    .region(region)
                    .build();

            System.out.println("Created a S3Client using temp credentials.");
            System.out.println("Listing objects in " + bucketName);
            ListObjectsRequest listObjects = ListObjectsRequest.builder()
                    .bucket(bucketName)
                    .build();

            ListObjectsResponse response = s3Client.listObjects(listObjects);
            List<S3Object> objectList = response.contents();
            for (S3Object object : objectList) {
                System.out.println("The name of the key is " + object.key());
                System.out.println("The owner is " + object.owner());
            }
        } catch (StsException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } finally {
            if (stsClient != null) {
                stsClient.close();
            }
            if (s3Client != null) {
                s3Client.close();
            }
        }
    }
}