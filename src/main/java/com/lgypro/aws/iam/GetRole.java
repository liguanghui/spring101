package com.lgypro.aws.iam;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.GetRoleRequest;
import software.amazon.awssdk.services.iam.model.GetRoleResponse;
import software.amazon.awssdk.services.iam.model.Role;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class GetRole {
    public static void main(String[] args) {
        String profileName = "default";
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a role name: ");
        String roleName = scanner.nextLine();
        if (roleName.isEmpty()) {
            roleName = "admin";
        }
        Region region = Region.AWS_GLOBAL;
        IamClient iamClient = IamClient.builder()
                .region(region)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
        getRoleInformation(iamClient, roleName);
        iamClient.close();
    }

    public static void getRoleInformation(IamClient iamClient, String roleName) {
        GetRoleRequest getRoleRequest = GetRoleRequest.builder()
                .roleName(roleName)
                .build();
        GetRoleResponse response = iamClient.getRole(getRoleRequest);
        Role role = response.role();
        System.out.println(role);
        String rolePolicyDocument = URLDecoder.decode(role.assumeRolePolicyDocument(), StandardCharsets.UTF_8);
        System.out.println(new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(JsonParser
                        .parseString(rolePolicyDocument)
                        .getAsJsonObject()));
    }
}
