package com.lgypro.aws.iam;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.ListRolesRequest;
import software.amazon.awssdk.services.iam.model.ListRolesResponse;
import software.amazon.awssdk.services.iam.model.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ListRoles {
    public static void main(String[] args) {
        String profileName = "default";
        IamClient iamClient = DependencyFactory.iamClient(profileName);
        List<Role> roles = listRoles(iamClient);
        System.out.printf("总共有%d个角色%n", roles.size());
        roles.stream().map(Role::arn).forEach(System.out::println);
    }

    public static List<Role> listRoles(IamClient iamClient) {
        return listRoles(iamClient, role -> true);
    }

    public static List<Role> listRoles(IamClient iamClient, Predicate<Role> filter) {
        boolean done = false;
        String newMarker = null;
        ListRolesRequest request = null;
        ListRolesResponse response = null;
        List<Role> roleList = new ArrayList<>();
        while (!done) {
            if (newMarker == null) {
                request = ListRolesRequest.builder().build();
            } else {
                request = ListRolesRequest.builder().marker(newMarker).build();
            }
            response = iamClient.listRoles(request);
            if (response.hasRoles()) {
                roleList.addAll(response.roles());
            }
            if (!response.isTruncated()) {
                done = true;
            } else {
                newMarker = response.marker();
            }
        }
        return roleList.stream().filter(filter).toList();
    }
}
