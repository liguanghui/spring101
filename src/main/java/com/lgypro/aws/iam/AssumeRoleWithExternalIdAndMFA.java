package com.lgypro.aws.iam;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.StorageClass;
import software.amazon.awssdk.services.sts.StsClient;
import software.amazon.awssdk.services.sts.model.AssumeRoleRequest;
import software.amazon.awssdk.services.sts.model.AssumeRoleResponse;
import software.amazon.awssdk.services.sts.model.Credentials;
import software.amazon.awssdk.services.sts.model.GetCallerIdentityResponse;

import java.io.File;
import java.util.Scanner;

/*
验证https://docs.aws.amazon.com/zh_cn/IAM/latest/UserGuide/id_roles_common-scenarios_aws-accounts.html#id_roles_common-scenarios_aws-accounts-example
 */
public class AssumeRoleWithExternalIdAndMFA {
    public static void main(String[] args) {
        String userName = "Bob";
        String accessKeyId = "AKIAVAXLPKUELVHH4XVH";
        String secretAccessKey = "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";
        /*
        {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::345164961032:root"
            },
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "sts:ExternalId": "EA325ADF-3ECE-4A91-AEF7-A82E25F0EDDD"
                },
                "Bool": {
                    "aws:MultiFactorAuthPresent": "true"
                }
            }
        }
    ]
}
         */
        String roleArn = "arn:aws:iam::894326332565:role/UpdateApp";
        String roleSessionName = "updateApp";
        String bucketName = "lgypro-mydemobucket";
        String objectKey = "vimclass.zip";
        String objectPath = System.getenv("HOME") + "/Downloads/" + objectKey;
        String externalId = "EA325ADF-3ECE-4A91-AEF7-A82E25F0EDDD";
        String serialNumber = "arn:aws:iam::345164961032:mfa/oneplus8";


        AwsBasicCredentials credentials = AwsBasicCredentials.create(accessKeyId, secretAccessKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        StsClient stsClient = StsClient.builder()
                .region(Region.AP_NORTHEAST_1)
                .credentialsProvider(credentialsProvider)
                .build();
        whoAmI(stsClient);
        System.out.printf("准备担任角色%s了，请输入%s的MFA tokenCode: ", roleArn, userName);
        String tokenCode = new Scanner(System.in).next();
        AwsCredentialsProvider newCredentialsProvider = assumeRole(stsClient, roleArn, roleSessionName, externalId, serialNumber, tokenCode);
        stsClient.close();
        S3Client s3Client = S3Client.builder()
                .region(Region.AP_NORTHEAST_1)
                .credentialsProvider(newCredentialsProvider)
                .build();
        putObject(s3Client, bucketName, objectKey, objectPath);
        s3Client.close();
    }

    public static void whoAmI(StsClient stsClient) {
        GetCallerIdentityResponse response = stsClient.getCallerIdentity();
        System.out.printf("userId: %s%n", response.userId());
        System.out.printf("account: %s%n", response.account());
        System.out.printf("arn: %s%n", response.arn());
    }

    public static AwsCredentialsProvider assumeRole(StsClient stsClient, String roleArn, String roleSessionName, String externalId, String serialNumber, String tokenCode) {
        AssumeRoleRequest request = AssumeRoleRequest.builder()
                .roleArn(roleArn)
                .roleSessionName(roleSessionName)
                .externalId(externalId)
                .serialNumber(serialNumber)
                .tokenCode(tokenCode)
                .build();
        AssumeRoleResponse response = stsClient.assumeRole(request);
        Credentials credentials = response.credentials();
        return StaticCredentialsProvider.create(
                AwsSessionCredentials.create(
                        credentials.accessKeyId(),
                        credentials.secretAccessKey(),
                        credentials.sessionToken()
                )
        );
    }

    public static void putObject(S3Client s3Client, String bucketName, String objectKey, String objectPath) {
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(objectKey)
                .storageClass(StorageClass.INTELLIGENT_TIERING)
                .build();
        s3Client.putObject(request, RequestBody.fromFile(new File(objectPath)));
        System.out.println("Successfully placed " + objectKey + " into bucket " + bucketName);
    }
}
