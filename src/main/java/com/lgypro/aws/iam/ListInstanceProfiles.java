package com.lgypro.aws.iam;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.InstanceProfile;
import software.amazon.awssdk.services.iam.model.ListInstanceProfilesResponse;

import java.util.Collections;
import java.util.List;

public class ListInstanceProfiles {
    public static void main(String[] args) {
        String profileName = "default";
        IamClient iamClient = DependencyFactory.iamClient(profileName);
        List<InstanceProfile> instanceProfiles = listInstanceProfiles(iamClient);
        System.out.printf("总共有%d个instance profile%n", instanceProfiles.size());
        instanceProfiles.forEach(instanceProfile -> {
            String name = instanceProfile.instanceProfileName();
            String roleName = instanceProfile.roles().get(0).roleName();
            System.out.println(name + ": " + roleName);
        });
    }

    public static List<InstanceProfile> listInstanceProfiles(IamClient iamClient) {
        ListInstanceProfilesResponse response = iamClient.listInstanceProfiles();
        if (response.hasInstanceProfiles()) {
            return response.instanceProfiles();
        }
        return Collections.emptyList();
    }
}
