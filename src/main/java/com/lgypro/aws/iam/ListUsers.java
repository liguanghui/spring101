package com.lgypro.aws.iam;

import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.ListUsersRequest;
import software.amazon.awssdk.services.iam.model.ListUsersResponse;

public class ListUsers {
    public static void main(String[] args) {
        String profileName = "demo-role";
        AwsCredentialsProvider credentialsProvider = ProfileCredentialsProvider.create(profileName);
        IamClient iamClient = IamClient.builder()
                .region(Region.AWS_GLOBAL)
                .credentialsProvider(credentialsProvider)
                .build();
        System.out.println(iamClient);
        listAllUsers(iamClient);
        iamClient.close();
    }

    public static void listAllUsers(IamClient iamClient) {
        boolean done = false;
        String newMarker = null;
        while (!done) {
            ListUsersResponse response;
            if (newMarker == null) {
                ListUsersRequest request = ListUsersRequest.builder().build();
                response = iamClient.listUsers(request);
            } else {
                ListUsersRequest request = ListUsersRequest.builder()
                        .marker(newMarker)
                        .build();
                response = iamClient.listUsers(request);
            }
            response.users().forEach(System.out::println);
            if (!response.isTruncated()) {
                done = true;
            } else {
                newMarker = response.marker();
            }
        }
    }
}
