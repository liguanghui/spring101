package com.lgypro.aws.iam;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.*;
import software.amazon.awssdk.services.iam.waiters.IamWaiter;

public class CreateUser {
    public static void main(String[] args) {
        /*

        final String usage = """
                Usage:
                    <username>\s

                Where:
                    username - The name of the user to create.\s

                """;
        if (args.length != 1) {
            System.out.println(usage);
            System.exit(1);
        }


        String username = args[0];
         */
        String username = "JAMES";
        Region region = Region.AWS_GLOBAL;
        IamClient iam = DependencyFactory.iamClient();

        String result = createIAMUser(iam, username);
        System.out.println("Successfully created user: " + result);
        iam.close();
    }

    // snippet-start:[iam.java2.create_user.main]
    public static String createIAMUser(IamClient iam, String username) {

        try {
            // Create an IamWaiter object
            IamWaiter iamWaiter = iam.waiter();

            CreateUserRequest request = CreateUserRequest.builder()
                    .userName(username)
                    .build();

            CreateUserResponse response = iam.createUser(request);

            // Wait until the user is created
            GetUserRequest userRequest = GetUserRequest.builder()
                    .userName(response.user().userName())
                    .build();

            WaiterResponse<GetUserResponse> waitUntilUserExists = iamWaiter.waitUntilUserExists(userRequest);
            waitUntilUserExists.matched().response().ifPresent(System.out::println);
            return response.user().userName();

        } catch (IamException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return "";
    }
    // snippet-end:[iam.java2.create_user.main]
}
