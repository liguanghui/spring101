package com.lgypro.aws.iam;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.*;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/*
aws iam get-policy --policy-arn arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity
aws iam list-entities-for-policy --policy-arn arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity
aws iam list-policy-tags --policy-arn arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity
aws iam list-policy-versions  --policy-arn arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity
aws iam get-policy-version  --policy-arn arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity --version-id v1
 */
public class GetPolicyInformation {
    public static void main(String[] args) {
        String profileName = "demo-role";
        String policyArn = "arn:aws:iam::894326332565:policy/DenyPrincipalGetCallerIdentity";
        Region region = Region.AWS_GLOBAL;
        IamClient iamClient = IamClient.builder()
                .region(region)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
        System.out.println("1. get policy metadata");
        Policy policy = getIamPolicy(iamClient, policyArn);
        System.out.println(policy);
        System.out.println("2. list entities for policy");
        listEntitiesForPolicy(iamClient, policyArn);
        System.out.println("3. list policy tags");
        listPolicyTags(iamClient, policyArn);
        System.out.println("4. list policy versions");
        listPolicyVersion(iamClient, policyArn);
        System.out.println("5. get policy default version");
        getPolicyVersion(iamClient, policyArn, policy.defaultVersionId());
        System.out.println("Done");
        iamClient.close();
    }

    public static Policy getIamPolicy(IamClient iamClient, String policyArn) {
        GetPolicyRequest request = GetPolicyRequest.builder()
                .policyArn(policyArn)
                .build();
        GetPolicyResponse response = iamClient.getPolicy(request);
        return response.policy();
    }

    public static void getPolicyVersion(IamClient iamClient, String policyArn, String policyVersionId) {
        GetPolicyVersionRequest request = GetPolicyVersionRequest.builder()
                .policyArn(policyArn)
                .versionId(policyVersionId)
                .build();
        GetPolicyVersionResponse response = iamClient.getPolicyVersion(request);
        PolicyVersion policyVersion = response.policyVersion();
        // System.out.println(policyVersion);
        String decodedDocument = URLDecoder.decode(policyVersion.document(), StandardCharsets.UTF_8);
        System.out.println(decodedDocument);
    }

    public static void listEntitiesForPolicy(IamClient iamClient, String policyArn) {
        ListEntitiesForPolicyRequest request = ListEntitiesForPolicyRequest.builder()
                .policyArn(policyArn)
                .build();
        ListEntitiesForPolicyResponse response = iamClient.listEntitiesForPolicy(request);
        if ((!response.hasPolicyGroups() || response.policyGroups().isEmpty()) &&
                (!response.hasPolicyUsers() || response.policyUsers().isEmpty()) &&
                (!response.hasPolicyRoles() || response.policyRoles().isEmpty())) {
            System.out.println("The is no entity attached with policy " + policyArn);
            return;
        }
        if (response.hasPolicyGroups()) {
            response.policyGroups().forEach(System.out::println);
        }
        if (response.hasPolicyUsers()) {
            response.policyUsers().forEach(System.out::println);
        }
        if (response.hasPolicyRoles()) {
            response.policyRoles().forEach(System.out::println);
        }
    }

    public static void listPolicyTags(IamClient iamClient, String policyArn) {
        ListPolicyTagsRequest request = ListPolicyTagsRequest.builder()
                .policyArn(policyArn)
                .build();
        ListPolicyTagsResponse response = iamClient.listPolicyTags(request);
        if (!response.hasTags() || response.tags().isEmpty()) {
            System.out.println("There is no tag associated with policy" + policyArn);
            return;
        }
        response.tags().forEach(System.out::println);
    }

    public static void listPolicyVersion(IamClient iamClient, String policyArn) {
        ListPolicyVersionsRequest request = ListPolicyVersionsRequest.builder()
                .policyArn(policyArn)
                .build();
        ListPolicyVersionsResponse response = iamClient.listPolicyVersions(request);
        if (!response.hasVersions() || response.versions().isEmpty()) {
            System.out.println("The is no policy version with policy " + policyArn);
        }
        response.versions().forEach(System.out::println);
    }
}
