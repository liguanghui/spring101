package com.lgypro.aws.iam;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.ListPoliciesRequest;
import software.amazon.awssdk.services.iam.model.ListPoliciesResponse;
import software.amazon.awssdk.services.iam.model.Policy;

import java.util.ArrayList;
import java.util.List;

public class ListPolicies {
    public static void main(String[] args) {
        String profileName = "demo-role";
        Region region = Region.AWS_GLOBAL;
        IamClient iamClient = IamClient.builder()
                .region(region)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
        List<Policy> policyList = listPolicies(iamClient);
        System.out.printf("总共有%d条策略%n", policyList.size());
        policyList.stream().map(Policy::policyName).forEach(System.out::println);
    }

    public static List<Policy> listPolicies(IamClient iamClient) {
        boolean done = false;
        String marker = null;
        ListPoliciesRequest request = null;
        ListPoliciesResponse response = null;
        List<Policy> policyList = new ArrayList<>();
        while (!done) {
            if (marker == null) {
                request = ListPoliciesRequest.builder()
                        .build();
            } else {
                request = ListPoliciesRequest.builder().marker(marker).build();
            }
            response = iamClient.listPolicies(request);
            if (response.hasPolicies()) {
                policyList.addAll(response.policies());
            }
            if (!response.isTruncated()) {
                done = true;
            } else {
                marker = response.marker();
            }
        }
        return policyList;
    }
}
