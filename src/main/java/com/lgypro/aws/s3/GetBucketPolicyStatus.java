package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;

import java.util.List;

/*
aws s3api get-bucket-policy --bucket lgypro-mydemobucket --query Policy --output text | jq .
 */
public class GetBucketPolicyStatus {
    public static void main(String[] args) {
        String profileName = "default";
        S3Client s3Client = DependencyFactory.s3Client(Region.AP_NORTHEAST_1, profileName);
        List<Bucket> buckets = ListBuckets.listBuckets(s3Client);
        buckets.forEach(bucket -> {
            String name = bucket.name();
            String bucketLocation = GetBucketInformation.getBucketLocation(s3Client, name);
            boolean bucketPolicyStatus = GetBucketInformation.getBucketPolicyStatus(
                    DependencyFactory.s3Client(Region.of(bucketLocation), profileName)
                    , name);
            System.out.printf("name: %s, isPublic: %s%n", name, bucketPolicyStatus);
        });
    }
}
