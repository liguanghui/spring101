package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.ListBucketsRequest;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

public class ListBuckets {
    public static void main(String[] args) {
        String profileName = "default";
        Region region = Region.AP_NORTHEAST_1;
        S3Client s3Client = DependencyFactory.s3Client(region, profileName);
        List<Bucket> buckets = listBuckets(s3Client);
        if (!buckets.isEmpty()) {
            System.out.println("name\tbucketLocation\tcreationDate");
        }
        buckets.forEach(bucket -> {
            String name = bucket.name();
            Instant creationDate = bucket.creationDate();
            String bucketLocation = GetBucketInformation.getBucketLocation(s3Client, name);
            String objectOwnership = GetBucketInformation.getBucketOwnershipControls(
                    DependencyFactory.s3Client(Region.of(bucketLocation), profileName), name);
            System.out.printf("%s\t%s\t%s\t%s%n", name, bucketLocation, creationDate, objectOwnership);
        });

        s3Client.close();
    }

    public static List<Bucket> listBuckets(S3Client s3Client) {
        ListBucketsRequest request = ListBucketsRequest.builder().build();
        ListBucketsResponse response = s3Client.listBuckets(request);
        if (response.hasBuckets()) {
            return response.buckets();
        }
        return Collections.emptyList();
    }
}
