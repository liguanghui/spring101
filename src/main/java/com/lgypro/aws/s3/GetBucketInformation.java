package com.lgypro.aws.s3;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.util.List;

public class GetBucketInformation {
    public static void main(String[] args) {
        S3Client s3Client = DependencyFactory.s3Client(Region.AP_NORTHEAST_1, "default");
        String bucketName = "security-vaults";
        String location = getBucketLocation(s3Client, bucketName);
        System.out.println(location);

    }

    public static String getBucketLocation(S3Client s3Client, String bucketName) {
        GetBucketLocationRequest request = GetBucketLocationRequest.builder()
                .bucket(bucketName)
                .build();
        GetBucketLocationResponse response = s3Client.getBucketLocation(request);
        return response.locationConstraintAsString();
    }

    public static void getBucketAccelerateConfiguration(S3Client s3Client, String bucketName) {
        GetBucketAccelerateConfigurationRequest request = GetBucketAccelerateConfigurationRequest.builder()
                .bucket(bucketName)
                .build();
        GetBucketAccelerateConfigurationResponse response = s3Client.getBucketAccelerateConfiguration(request);
        String status = response.statusAsString();
        System.out.println("Transfer Acceleration: " +
                (status == null ? "Disabled" : status));
    }

    public static void getBucketAcl(S3Client s3Client, String bucketName) {
        GetBucketAclRequest request = GetBucketAclRequest.builder()
                .bucket(bucketName)
                .build();
        GetBucketAclResponse response = s3Client.getBucketAcl(request);
        Owner owner = response.owner();
        List<Grant> grants = response.grants();
        System.out.println(owner);
        grants.forEach(grant -> {
            System.out.println(grant.grantee() + "\t" + grant.permissionAsString());
        });
    }

    /**
     * aws s3api get-bucket-ownership-controls --bucket lgypro-mydemobucket
     */
    public static String getBucketOwnershipControls(S3Client s3Client, String bucketName) {
        GetBucketOwnershipControlsRequest request = GetBucketOwnershipControlsRequest.builder().bucket(bucketName).build();
        GetBucketOwnershipControlsResponse response = s3Client.getBucketOwnershipControls(request);
        return response.ownershipControls().rules().get(0).objectOwnershipAsString();
    }

    public static String getBucketPolicy(S3Client s3Client, String bucketName) {
        GetBucketPolicyRequest request = GetBucketPolicyRequest.builder().bucket(bucketName).build();
        try {
            GetBucketPolicyResponse response = s3Client.getBucketPolicy(request);
            return new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(
                            JsonParser.parseString(response.policy()).getAsJsonObject()
                    );
        } catch (S3Exception e) {
            if (e.getMessage().startsWith("The bucket policy does not exist")) {
                return "The bucket policy does not exist";
            }
            throw e;
        }
    }

    public static boolean getBucketPolicyStatus(S3Client s3Client, String bucketName) {
        GetBucketPolicyStatusRequest request = GetBucketPolicyStatusRequest.builder().bucket(bucketName).build();
        try {
            GetBucketPolicyStatusResponse response = s3Client.getBucketPolicyStatus(request);
            return response.policyStatus().isPublic();
        } catch (S3Exception e) {
            if (e.getMessage().startsWith("The bucket policy does not exist")) {
                return false;
            }
            throw e;
        }
    }

    public static PublicAccessBlockConfiguration getBucketPublicAccessBlock(S3Client s3Client, String bucketName) {
        GetPublicAccessBlockRequest request = GetPublicAccessBlockRequest.builder()
                .bucket(bucketName).build();
        return s3Client.getPublicAccessBlock(request).publicAccessBlockConfiguration();
    }
}

