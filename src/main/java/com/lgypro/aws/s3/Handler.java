package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

public class Handler {
    private final S3Client s3Client;

    public Handler() {
        s3Client = DependencyFactory.s3Client();
    }

    public void sendRequest() {
        String bucket = "lgypro-train";
        String key = "key.txt";
        s3Client.putObject(PutObjectRequest.builder().bucket(bucket).key(key)
                        .build(),
                RequestBody.fromString("Testing with the {sdk-java}\n"));

        System.out.println("Upload complete");
        System.out.printf("%n");
        s3Client.close();
    }
}
