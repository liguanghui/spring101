package com.lgypro.aws.s3;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.util.List;

public class ListBucketObjects {
    public static void main(String[] args) {
        String profileName = "default";
        /// String bucketName = "lgypro-train";
        String bucketName = "aws-cloudtrail-logs-345164961032-7fb8bf1a";
        S3Client s3Client = S3Client.builder()
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .region(Region.AP_NORTHEAST_2)
                .build();
        String delimiter = "/";
        String prefix = "AWSLogs/o-x2mmwfottl/345164961032/CloudTrail/";
        listBucketObjects(s3Client, bucketName, delimiter, prefix);
    }

    public static void listBucketObjects(S3Client s3Client, String bucketName, String delimiter, String prefix) {
        ListObjectsV2Request request = ListObjectsV2Request.builder()
                .bucket(bucketName)
                .encodingType(EncodingType.URL)
                .delimiter(delimiter)
                .prefix(prefix)
                .fetchOwner(true)
                .build();
        ListObjectsV2Response response = s3Client.listObjectsV2(request);
        List<S3Object> contents = response.contents();
        contents.forEach(object -> {
            String key = object.key();
            Long size = object.size();
            ObjectStorageClass objectStorageClass = object.storageClass();
            Owner owner = object.owner();
            System.out.printf("name: %s, storageClass: %s, size: %d KB, owner: %s%n", key, objectStorageClass, size / 1024, owner);
        });
        List<CommonPrefix> commonPrefixes = response.commonPrefixes();
        commonPrefixes.stream().map(CommonPrefix::prefix).forEach(System.out::println);
    }
}
