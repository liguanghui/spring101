package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetBucketVersioningRequest;
import software.amazon.awssdk.services.s3.model.GetBucketVersioningResponse;

/*
aws s3api get-bucket-versioning --bucket lgypro-first-bucket
 */
public class GetBucketVersioningConfiguration {
    public static void main(String[] args) {
        S3Client s3Client = DependencyFactory.s3Client(Region.AP_NORTHEAST_1, "default");
        String bucketName = "mysoulmyheart";
        // String bucketName = "lgypro-first-bucket";
        try (s3Client) {
            String status = retrieveBucketVersioningConfiguration(s3Client, bucketName);
            System.out.println("status = " + ((status != null) ? status : "unversioned"));
        }
    }

    public static String retrieveBucketVersioningConfiguration(S3Client s3Client, String bucketName) {
        GetBucketVersioningRequest request = GetBucketVersioningRequest.builder().bucket(bucketName).build();
        GetBucketVersioningResponse response = s3Client.getBucketVersioning(request);
        return response.statusAsString();
    }
}
