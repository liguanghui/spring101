package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;

import java.time.Instant;

public class ListKeysVersioningEnabledBucket {
    private static long versionCount = 0;

    public static void main(String[] args) {
        String bucketName = "lgypro-train";

        S3Client s3Client = DependencyFactory.s3Client(Region.AP_NORTHEAST_2, "default");

        // Retrieve the list of versions. If the bucket contains more versions
        // than the specified maximum number of results, Amazon S3 returns
        // one page of results per request.
        ListObjectVersionsRequest request = ListObjectVersionsRequest.builder().bucket(bucketName).maxKeys(2).build();
        ListObjectVersionsResponse response = s3Client.listObjectVersions(request);
        while (true) {
            if (response.hasVersions()) {
                response.versions().forEach(version -> {
                    versionCount++;
                    String key = version.key();
                    String etag = version.eTag();
                    Boolean isLatest = version.isLatest();
                    Instant instant = version.lastModified();
                    Long size = version.size();
                    String versionId = version.versionId();
                    String storageClass = version.storageClassAsString();
                    System.out.printf("key = %s, versionId = %s, etag = %s, isLatest = %s, instant = %s, size = %d, storageClass = %s%n",
                            key, versionId, etag, isLatest, instant, size, storageClass);
                });
            }
            if (response.hasDeleteMarkers()) {
                response.deleteMarkers().forEach(deleteMarker -> {
                    versionCount++;
                    String key = deleteMarker.key();
                    Instant instant = deleteMarker.lastModified();
                    Boolean isLatest = deleteMarker.isLatest();
                    String versionId = deleteMarker.versionId();
                    System.out.printf("key = %s, versionId = %s, instant = %s, isLatest = %s%n",
                            key, versionId, instant, isLatest);
                });
            }
            if (response.isTruncated()) {
                request = ListObjectVersionsRequest.builder().bucket(bucketName).maxKeys(2).keyMarker(response.nextKeyMarker()).versionIdMarker(response.nextVersionIdMarker()).build();
                response = s3Client.listObjectVersions(request);
            } else {
                break;
            }
        }
        System.out.println("versionCount = " + versionCount);
    }
}
