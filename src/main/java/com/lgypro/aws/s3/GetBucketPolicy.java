package com.lgypro.aws.s3;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

/*
aws s3api get-bucket-policy --bucket lgypro-mydemobucket --query Policy --output text | jq .
 */
public class GetBucketPolicy {
    public static void main(String[] args) {
        String profileName = "demo-role";
        S3Client s3Client = S3Client.builder()
                .region(Region.AP_NORTHEAST_1)
                .credentialsProvider(ProfileCredentialsProvider.create(profileName))
                .build();
        String bucketName = "lgypro-mydemobucket";
        String policy = GetBucketInformation.getBucketPolicy(s3Client, bucketName);
        System.out.println(policy);
    }
}
