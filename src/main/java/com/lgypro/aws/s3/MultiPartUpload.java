package com.lgypro.aws.s3;

import com.lgypro.aws.DependencyFactory;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MultiPartUpload {
    public static void main(String[] args) throws IOException {
        S3Client s3Client = DependencyFactory.s3Client(Region.AP_NORTHEAST_2, "default");
        String bucketName = "lgypro-train";
        System.out.print("请输入要上传的文件名： ");
        String filename = new Scanner(System.in).nextLine();
        String key = new File(filename).getName();
        multipartUpload(s3Client, bucketName, key, filename);
    }

    public static void multipartUpload(S3Client s3Client, String bucketName, String key, String filename) throws IOException {
        int partSize = 5 * 1024 * 1024;
        // snippet-start:[s3.java2.s3_object_operations.upload_multi_part]
        // First create a multipart upload and get the upload id
        CreateMultipartUploadRequest createMultipartUploadRequest = CreateMultipartUploadRequest.builder()
                .bucket(bucketName)
                .key(key)
                .storageClass(StorageClass.INTELLIGENT_TIERING)
                .build();
        CreateMultipartUploadResponse response = s3Client.createMultipartUpload(createMultipartUploadRequest);
        String uploadId = response.uploadId();
        System.out.println(uploadId);
        BufferedInputStream byteStream = new BufferedInputStream(new FileInputStream(filename));
        byte[] buffer = new byte[partSize];
        int readBytes = byteStream.read(buffer);
        int partNumber = 1;
        List<CompletedPart> completedPartList = new ArrayList<>();
        while (readBytes > 0) {
            // Upload all the different parts of the object
            UploadPartRequest uploadPartRequest = UploadPartRequest.builder()
                    .bucket(bucketName)
                    .key(key)
                    .uploadId(uploadId)
                    .partNumber(partNumber).build();
            String etag = s3Client.uploadPart(uploadPartRequest, RequestBody.fromByteBuffer(ByteBuffer.wrap(buffer, 0, readBytes))).eTag();
            CompletedPart part = CompletedPart.builder().partNumber(partNumber).eTag(etag).build();
            completedPartList.add(part);
            System.out.printf("finish part %d, size is %d Bytes%n", partNumber, readBytes);
            partNumber++;
            readBytes = byteStream.read(buffer);
        }
        // Finally call completeMultipartUpload operation to tell S3 to merge all uploaded
        // parts and finish the multipart operation.
        CompletedMultipartUpload completedMultipartUpload = CompletedMultipartUpload.builder()
                .parts(completedPartList)
                .build();
        CompleteMultipartUploadRequest completeMultipartUploadRequest =
                CompleteMultipartUploadRequest.builder()
                        .bucket(bucketName)
                        .key(key)
                        .uploadId(uploadId)
                        .multipartUpload(completedMultipartUpload)
                        .build();
        s3Client.completeMultipartUpload(completeMultipartUploadRequest);
    }
}
