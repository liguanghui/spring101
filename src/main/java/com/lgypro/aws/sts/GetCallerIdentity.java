package com.lgypro.aws.sts;


import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sts.StsClient;
import software.amazon.awssdk.services.sts.model.GetCallerIdentityResponse;

public class GetCallerIdentity {
    public static void main(String[] args) {
        Region region = Region.AP_NORTHEAST_1;
        String profileName = "demo-role";
        AwsCredentialsProvider credentialsProvider = ProfileCredentialsProvider.create(profileName);
        AwsCredentials awsCredentials = credentialsProvider.resolveCredentials();
        System.out.printf("ACCESS_KEY_ID=%s%nSECRET_ACCESS_KEY=%s%n", awsCredentials.accessKeyId(), awsCredentials.secretAccessKey());
        StsClient stsClient = StsClient.builder()
                .region(region)
                .credentialsProvider(credentialsProvider)
                .build();
        getCallerIdentity(stsClient);
    }

    public static void getCallerIdentity(StsClient stsClient) {
        GetCallerIdentityResponse response = stsClient.getCallerIdentity();
        System.out.printf("userId: %s%n", response.userId());
        System.out.printf("account: %s%n", response.account());
        System.out.printf("arn: %s%n", response.arn());
    }

}
