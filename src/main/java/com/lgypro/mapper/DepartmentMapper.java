package com.lgypro.mapper;

import com.lgypro.entity.Department;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

@CacheNamespace(readWrite = false)
public interface DepartmentMapper {
  Department findExactlyById(@Param("id") int id);

  Department findDepartmentAndEmployees(@Param("id") int id);
}
