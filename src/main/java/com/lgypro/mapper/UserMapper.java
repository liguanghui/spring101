package com.lgypro.mapper;

import com.lgypro.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    List<User> findAll();

    User getUserByName(String username);

    User checkCredential(@Param("username") String username, @Param("password") String password);

    User checkCredentialByMap(Map<String, String> map);

    int insertUser(User user);

    User getById(@Param("id") int id);

    int getCount();

    Map<String, Object> getByIdToMap(@Param("id") int id);

    List<Map<String, Object>> getAllToMap();

    @MapKey("id")
    Map<Integer, Map<String, Object>> getAllToMap2();

    @Select("select * from users where username = #{username} and password = #{password}")
    User auth(Map<String, String> loginCredential);

    @Delete("delete from users where id in (${ids})")
    int deleteUsersByCommaSeparatedIds(@Param("ids") String commaSeparatedIds);
}
