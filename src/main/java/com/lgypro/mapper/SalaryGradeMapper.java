package com.lgypro.mapper;

import com.lgypro.entity.SalaryGrade;
import org.apache.ibatis.annotations.Param;

public interface SalaryGradeMapper {
    SalaryGrade getGradeFromSalary(@Param("salary") int salary);
}
