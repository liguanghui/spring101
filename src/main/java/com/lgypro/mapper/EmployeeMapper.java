package com.lgypro.mapper;

import com.lgypro.entity.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@CacheNamespaceRef(EmployeeMapper.class)
public interface EmployeeMapper {
    @Select("select * from emp")
    @ResultMap("employeeResultMap")
    List<Employee> getAllEmployees();

    Employee findEmployeeExactlyByEmpNo(@Param("empno") int empno);

    List<Employee> findEmployeesByDepartmentId(@Param("departmentId") int departmentId);

    List<Employee> findEmployeesByNameUsingLikeOperator(@Param("name") String name);

    List<Employee> findEmployeesByNameUsingRegularExpression(@Param("name") String name);

    @Insert("""
            insert into emp (empno, ename, job, mgr, hiredate, sal, comm, deptno) values
            (#{id}, #{name}, #{job}, #{managerId}, #{hireDate}, #{salary}, #{commission}, #{departmentId})
            """)
    void insertEmployee(Map<String, Object> properties);
}
