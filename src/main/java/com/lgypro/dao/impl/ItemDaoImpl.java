package com.lgypro.dao.impl;

import com.lgypro.dao.ItemDao;
import com.lgypro.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

public class ItemDaoImpl implements ItemDao {

    @Autowired
    private UserDao userDao;

    @Override
    public String toString() {
        return "ItemDaoImpl{" +
                "userDao=" + userDao +
                '}';
    }
}
