package com.lgypro.dao.impl;

import com.lgypro.dao.UserDao;
import lombok.Data;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    @Override
    public void close() {
        System.out.println(this + " will be closed soom");
    }
}
