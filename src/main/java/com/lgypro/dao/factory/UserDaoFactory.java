package com.lgypro.dao.factory;

import com.lgypro.dao.UserDao;
import com.lgypro.dao.impl.UserDaoImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;

@Slf4j
public class UserDaoFactory implements FactoryBean<UserDao>, DisposableBean {

    public UserDaoFactory() {
        log.debug("Constructing UserDaoFactory instance");
    }

    private static volatile UserDao INSTANCE;

    @Override
    public UserDao getObject() throws Exception {
        log.debug("Invoking getObject() of " + super.toString());
        if (INSTANCE == null) {
            synchronized (this) {
                if (INSTANCE == null) {
                    INSTANCE = new UserDaoImpl();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public Class<?> getObjectType() {
        return UserDaoImpl.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void destroy() throws Exception {
        if (INSTANCE != null) {
            INSTANCE.close();
        }
    }
}
