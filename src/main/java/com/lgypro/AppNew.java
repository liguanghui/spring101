package com.lgypro;

import com.lgypro.config.SpringConfig;
import com.lgypro.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

@Slf4j
public class AppNew {
    public static void main(String[] args) {
        // AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.registerShutdownHook();
//        log.debug("-------bean definitions are-------------");
//        Arrays.stream(context.getBeanDefinitionNames()).forEach(name -> {
//            log.debug("{} -> {}", name, context.getBeanDefinition(name).getBeanClassName());
//        });
        log.debug("-----bean post processors are---------------");
        context.getBeansOfType(BeanPostProcessor.class).forEach((BeanName, bean) -> {
            log.debug("{} -> {}", BeanName, bean.getClass().getName());
        });
        log.debug("-----bean factory post processors are---------------");
        context.getBeansOfType(BeanFactoryPostProcessor.class).forEach((BeanName, bean) -> {
            log.debug("{} -> {}", BeanName, bean.getClass().getName());
        });
        UserService userService = context.getBean(UserService.class);
        System.out.println(userService);
    }
}
