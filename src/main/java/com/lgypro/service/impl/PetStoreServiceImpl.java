package com.lgypro.service.impl;

import com.lgypro.dao.AccountDao;
import com.lgypro.dao.ItemDao;
import com.lgypro.dao.UserDao;
import com.lgypro.service.PetStoreService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PetStoreServiceImpl implements PetStoreService {
    private AccountDao accountDao;
    private ItemDao itemDao;

    private UserDao userDao;

    public PetStoreServiceImpl() {
        log.debug("Constructing " + this.getClass().getName());
    }

    public void setAccountDao(AccountDao accountDao) {
        log.debug("Setting attribute accountDao of " + super.toString());
        this.accountDao = accountDao;
    }

    public void setItemDao(ItemDao itemDao) {
        log.debug("Setting attribute itemDao of " + super.toString());
        this.itemDao = itemDao;
    }

    public void setUserDao(UserDao userDao) {
        log.debug("Setting attribute userDao of " + super.toString());
        this.userDao = userDao;
    }

    @Override
    public String toString() {
        return "PetStoreServiceImpl{" +
                "accountDao=" + accountDao +
                ", itemDao=" + itemDao +
                ", userDao=" + userDao +
                '}';
    }
}
