package com.lgypro.service;

import com.lgypro.entity.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
}
