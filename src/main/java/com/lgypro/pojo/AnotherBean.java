package com.lgypro.pojo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class AnotherBean {
    public AnotherBean(YetAnotherBean yetAnotherBean) {
        this.yetAnotherBean = yetAnotherBean;
        log.info("AnotherBean创建了");
    }

    private YetAnotherBean yetAnotherBean;
    private int i;

    public void setIntegerProperty(int i) {
        log.info("为AnotherBean设置属性i");
        this.i = i;
    }

    public void init() {
        log.info("AnotherBean的init()方法");
    }
    private String URL;

    public void setURL(String URL) {
        log.info("为AnotherBean设置属性URL");
        this.URL = URL;
    }
}
