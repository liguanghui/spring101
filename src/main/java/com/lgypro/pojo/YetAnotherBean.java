package com.lgypro.pojo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class YetAnotherBean {
    private YetAnotherBean(String name) {
        log.info("YetAnotherBean的有参数构造方法");
    }

    public static YetAnotherBean createInstance(String name) {
        log.info("YetAnotherBean创建了");
        return new YetAnotherBean(name);
    }
}
