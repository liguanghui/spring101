package com.lgypro.pojo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ExampleBean {
    public ExampleBean() {
        log.info("ExampleBean创建了");
    }

    private AnotherBean anotherBean;
    private YetAnotherBean yetAnotherBean;

    public void setAnotherBean(AnotherBean anotherBean) {
        log.info("ExampleBean设置AnotherBean");
        this.anotherBean = anotherBean;
    }

    public void setYetAnotherBean(YetAnotherBean yetAnotherBean) {
        log.info("ExampleBean设置YetAnotherBean");
        this.yetAnotherBean = yetAnotherBean;
    }
}
