package com.lgypro;

import com.lgypro.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

@Slf4j
public class ApplicationContextTest {
    public static void main(String[] args) throws SQLException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        System.out.println("Spring Context initialized");
        System.out.println("------BeanPostProcessors are --------------");
        context.getBeansOfType(BeanPostProcessor.class).forEach((beanName, bean) -> {
            System.out.println(bean.getClass().getName());
        });
        System.out.println("------BeanFactoryPostProcessors are --------------");
        context.getBeansOfType(BeanFactoryPostProcessor.class).forEach((beanName, bean) -> {
            System.out.println(bean.getClass().getName());
        });
        System.out.println("--------------------");
        Arrays.stream(context.getBeanFactory().getBeanDefinitionNames()).forEach(System.out::println);
        System.out.println("--------------------");
        System.out.println(context.getBean(UserService.class));
        DataSource dataSource = context.getBean(DataSource.class);
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        Statement statement = connection.createStatement();
        System.out.println(statement);
        String sql = "select count(*) from emp";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            int count = resultSet.getInt(1);
            System.out.println("emp表总共有" + count + "条记录");
        }
        context.close();
    }
}
