package com.lgypro;

import com.lgypro.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

import java.util.Arrays;

@Slf4j
public class Main {
    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions("beans.xml");
        UserService userService = beanFactory.getBean(UserService.class);
        log.info("{}", userService);
        log.info("bean的个数是{}", beanFactory.getBeanDefinitionCount());
        Arrays.stream(beanFactory.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
