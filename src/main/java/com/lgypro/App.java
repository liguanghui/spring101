package com.lgypro;

import com.lgypro.entity.User;
import com.lgypro.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-mybatis.xml");
        UserService userService = context.getBean(UserService.class);
        List<User> userList = userService.findAll();
        userList.stream().map(User::getUsername).forEach(System.out::println);
    }
}
