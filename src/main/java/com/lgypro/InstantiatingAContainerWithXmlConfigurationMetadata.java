package com.lgypro;

import com.lgypro.internal.LoggingCapableApplicationStartup;
import com.lgypro.internal.ReplacingBeanFactoryApplicationStartupImplementationWithApplicationContext;
import com.lgypro.internal.processor.DummyBeanFactoryPostProcessor;
import com.lgypro.service.PetStoreService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InstantiatingAContainerWithXmlConfigurationMetadata {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
        context.setApplicationStartup(new LoggingCapableApplicationStartup());
        context.setConfigLocation("services.xml");
        context.addBeanFactoryPostProcessor(new ReplacingBeanFactoryApplicationStartupImplementationWithApplicationContext(context));
        context.addBeanFactoryPostProcessor(new DummyBeanFactoryPostProcessor());
        context.refresh();
        PetStoreService bean = context.getBean(PetStoreService.class);
        System.out.println(bean);
    }
}
