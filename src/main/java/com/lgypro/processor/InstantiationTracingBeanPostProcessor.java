package com.lgypro.processor;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class InstantiationTracingBeanPostProcessor implements BeanPostProcessor {
    public InstantiationTracingBeanPostProcessor() {
        System.out.println("InstantiationTracingBeanPostProcessor实例化了");
    }

    // simply return the instantiated bean as-is
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println("BeforeInitialization: Bean '" + beanName + "' created : " + bean.toString());
        return bean; // we could potentially return any object reference here...
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("AfterInitialization: Bean '" + beanName + "' created : " + bean.toString());
        return bean;
    }
}
